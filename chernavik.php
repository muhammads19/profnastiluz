<div class="container general-section">

    <div id="options" class="filter-menu">
        <ul class="option-set black nav nav-pills">
            <li class="filter active" data-filter="all">Барчаси</li>
            <?php foreach ($categories as $category): ?>
                <li class="filter" data-filter=".category<?= $category['id'] ?>"><?= $category['name_uz'] ?></li>
                <?php endforeach; ?>
        </ul>
    </div>

</div>
<div class="container general-section white">
    <div id="mix-container" class="portfolio-grid custom no-item-pad">
        
        <?php foreach ($images as $photo):?>
            <div class="grids col-xs-12 col-sm-4 col-md-3 mix all category<?= $photo['category_id']?>">
                  <div class="grid">
                <div style="width: 275px; height: 188px; overflow: hidden;position: relative;">
                    <img style="position: absolute; top:-100%; left:0; right: 0; bottom:-100%; margin: auto;" src="<?= Yii::getAlias('@web') ?>/files/photo/thumb/<?= $photo['file']?>" width="400" height="273" alt="Recent Work" class="img-responsive" />
                </div>
                    <div class="figcaption">
                        <h4>Tom-Baraka</h4>
                        <!-- Image Popup-->
                        <a href="<?= Yii::getAlias('@web')?>/files/photo/<?= $photo['file']?>" data-rel="prettyPhoto[portfolio]">
                            <i class="fa fa-search"></i>
                        </a> 
                        <a href="portfolio-single.html">
                            <i class="fa fa-link"></i>
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?> 
          <?=\yii\widgets\LinkPager::widget([
        'pagination' => $pagination
    ])?>
    </div>                 
</div>

<?


$category = Category::find();

        $wallpapers = Photo::find()->all();
        $wallpapers->groupBy('photo_id')->orderBy('created_at desc');
        $cloneWallpapers = clone $wallpapers;

        $pagination = new Pagination([
            'pageSize' => 32,
            'totalCount' => $cloneWallpapers->count()
        ]);

        $wallpapers = $wallpapers->offset($pagination->offset)->limit($pagination->limit)->all();
        $wallpapers2 = Photo::find()
           ->groupBy('photo_id')->orderBy('created_at desc')->all();
        $photo_ids = ArrayHelper::getColumn($wallpapers2, 'photo_id');

        $tags = MediaTag::find()->where(['media_id' => $media_ids])->groupBy('tag_id')->all();

        return $this->render('index', [
            'category' => $category,
            'wallpapers' => $wallpapers,
            'pagination' => $pagination,
            'tags' => $tags
        ]);?>

        <?= ImageResized::getResized('uploads/media/'.$wallpaper->filename, null, 200)?>



          <?= $form->field($model, 'verifyCode')->widget(Captcha::classname(), [
                'template' => '{image}{input}',
            ])?>



             <?= Html::submitButton('Юбориш', ['class' => 'btn btn-primary','name' => 'contact-button']) ?>





               