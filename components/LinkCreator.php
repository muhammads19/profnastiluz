<?php

namespace app\components;

use yii\helpers\Url;
use yii\base\Component;

/**
 * Description of Menu
 *
 * @author muhammad
 */
class LinkCreator extends Component {
    
    public function create($link){
        $url = NULL;
        
        if (strpos($link, 'http://') !== false || strpos($link, 'https://') !== false || strpos($link, 'ftp://') !== false) {
            $url = $link;
        } elseif($link) {
            //TODO for controller/action&id=5&name=ali
            $urlArray = explode('&', $link);
            $urlRoute = array_shift($urlArray);
            $urlParams = [];
            foreach ($urlArray as $urlItem) {
                $urlItemArray = explode('=', $urlItem);
                $urlParams[$urlItemArray[0]] = $urlItemArray[1];
            }
            array_unshift($urlParams, $urlRoute);
            $url = Url::to($urlParams);
        }
        
        return $url;
    }
    
    public function createArr($link){
        $url = NULL;
        
        if (strpos($link, 'http://') !== false || strpos($link, 'https://') !== false || strpos($link, 'ftp://') !== false) {
            $url = $link;
        } elseif($link) {
            //TODO for controller/action&id=5&name=ali
            $urlArray = explode('&', $link);
            $urlRoute = array_shift($urlArray);
            $urlParams = [];
            foreach ($urlArray as $urlItem) {
                $urlItemArray = explode('=', $urlItem);
                $urlParams[$urlItemArray[0]] = $urlItemArray[1];
            }
            array_unshift($urlParams, $urlRoute);
            $url = $urlParams;
        }
        
        return $url;
    }
}
