<?php

namespace app\components;

use yii\base\Widget;

/**
 * Description of Menu
 *
 * @author muhammad
 */
class Menu extends Widget{
    private $menus = [];
    
    public function init(){
        parent::init();
        $this->menus = (new \app\models\Menu)->menusAsTree;
    }
    
    public function run(){
        
        return $this->render('menu', [
            'menus' => $this->menus
        ]);
    }
}
