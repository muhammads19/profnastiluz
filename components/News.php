<?php
namespace app\components;


use yii\base\Widget;

class News extends \yii\base\Widget{
    public $news = [];

    public function init() {
        parent::init();
        $this->news = \app\models\News::find()->all();
    }

    public function run() {

        return $this->render('news', [
            'news' => $this->news
        ]);
    }

}
