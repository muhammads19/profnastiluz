<?php

use app\assets\JWPlayerAsset;

JWPlayerAsset::register($this);
?>
<?php if ($videos): ?>

    <div class="container">
        <div class="row">

            <?php foreach ($videos as $video): ?>

                <?php if ($video->source == 4): ?>
            
                    <div class="col-lg-4 col-sm-4 col-md-4" style="height: 180px;">
                        <div style="width: 100%; height: 175px;">
                            <div id="video<?= $video['id'] ?>"></div>
                        </div>

                        <script type="text/javascript">
                            <?php ob_start() ?>

                            // Create a jwplayer instance
                            jwplayer('video<?= $video['id'] ?>').setup({
                                file: '<?= Yii::getAlias('@web') ?>/files/video/<?= $video->file ?>',
                                image: '<?= Yii::getAlias('@web') ?>/files/video/<?= $video->img ?>',
                                    width: "100%",
                                    height: "100%"
                                });
                            <?php $this->registerJS(ob_get_clean()) ?>

                        </script>
                    </div>

                <?php else: ?>
                    
                    <div class="col-lg-4 col-sm-4 col-md-4" style="height: 180px;">
                        <iframe  style="width: 100%;" width="315" height="175" src="<?= $video->file; ?>" frameborder="0" allowfullscreen></iframe>
                    </div>

                <?php endif; ?>

            <?php endforeach; ?> 

        </div>

    </div>

<?php endif; ?>