<?php

$config = [
    'language' => 'uz',
    'id' => 'main',
    'defaultRoute' => 'site/index',
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'modules' => [
                'rbac' => [
                    'class' => 'yii2mod\rbac\Module',
                    'controllerMap' => [
                        'route' => [
                            'class' => 'yii2mod\rbac\controllers\RouteController',
                            'modelClass' => [
                                'class' => 'yii2mod\rbac\models\RouteModel',
                                'excludeModules' => ['debug', 'gii'],
                            ],
                        ],
                    ],
                ],
                'settings-storage' => [
                    'class' => 'yii2mod\settings\Module',
                ],
            ],
        ],
        'comment' => [
            'class' => 'yii2mod\comments\Module',
            'controllerMap' => [
                'manage' => [
                    'class' => 'yii2mod\comments\controllers\ManageController',
                    'layout' => '@app/modules/admin/views/layouts/column2',
                ],
            ],
        ],
        'cms' => [
            'class' => 'yii2mod\cms\Module',
            'controllerMap' => [
                'manage' => [
                    'class' => 'yii2mod\cms\controllers\ManageController',
                    'layout' => '@app/modules/admin/views/layouts/column2',
                ],
            ],
        ],
        'dynagrid'=>[
            'class'=>'\kartik\dynagrid\Module',
            // other settings (refer documentation)
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'components' => [

    'linkCreator' => [
            'class' => 'app\components\LinkCreator',
        ],
        'settings' => [
            'class' => 'yii2mod\settings\components\Settings',
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'name' => 'jkhew4dfL4Fn37Q377Sv'
        ],
        'user' => [
            'identityClass' => 'app\models\UserModel',
            'enableAutoLogin' => true,
            'on afterLogin' => function ($event) {
                $event->identity->updateLastLogin();
            },
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
             'enableLanguageDetection' => false,
             'enableDefaultLanguageUrlCode' => false,
              'languages' => ['uz', 'ru', 'en'],
            'rules' => [
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                ['class' => 'yii2mod\cms\components\PageUrlRule'],
            ],
        ],
    ],
];

return $config;
