<?php

namespace app\controllers;

use app\models\forms\ContactForm;
use app\models\forms\ResetPasswordForm;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii2mod\rbac\filters\AccessControl;
use app\models\Contact;



/**
 * Class SiteController
 *
 * @package app\controllers
 */
class TovarController extends Controller
{
     public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionProfnastil()

    {
        return $this->render('profnastil');
    }

     public function actionTunukapon()

    {
        return $this->render('tunukapon');
    }

       public function actionSendvich()

    {
        return $this->render('sendvich');
    }
       
       public function actionCherepitsa()

    {
        
        return $this->render('cherepitsa');

    }
       public function actionFasad()


    {
        return $this->render('fasad');

    }
        public function actionPanjara()


    {
        return $this->render('panjara');

    } 

    public function actionTunikafon()


    {
        return $this->render('tunikafon');

    }   

}
