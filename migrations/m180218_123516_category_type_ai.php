<?php

use yii\db\Migration;

/**
 * Class m180218_123516_category_type_ai
 */
class m180218_123516_category_type_ai extends Migration
{
    public function safeUp() {

        $this->execute("
            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

            ALTER TABLE `category_type` 
            CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;


            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180218_123516_category_type_ai cannot be reverted.\n";
        
        return false;
    }
}
