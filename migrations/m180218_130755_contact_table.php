<?php

use yii\db\Migration;

/**
 * Class m180218_130755_contact_table
 */
class m180218_130755_contact_table extends Migration
{

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->execute("

            CREATE TABLE `contact` (
                `id` int(11) UNSIGNED NOT NULL,
                `full_name` varchar(255) NOT NULL,
                `email` varchar(255) NOT NULL,
                `phone` varchar(255) NOT NULL,
                `message` text NOT NULL,
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            --
            -- Indexes for dumped tables
            --

            --
            -- Indexes for table `contact`
            --
            ALTER TABLE `contact`
                ADD PRIMARY KEY (`id`);

            --
            -- AUTO_INCREMENT for dumped tables
            --

            --
            -- AUTO_INCREMENT for table `contact`
            --
            ALTER TABLE `contact`
                MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
        ");
    }

    public function down()
    {
        $this->dropTable('contact');
    }
    
}
