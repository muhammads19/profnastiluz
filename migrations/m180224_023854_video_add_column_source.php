<?php

use yii\db\Migration;

/**
 * Class m180224_023854_video_add_column_source
 */
class m180224_023854_video_add_column_source extends Migration
{
    public function up()
    {
        $this->addColumn('{{%video}}', 'source', $this->string(30));
    }

    public function down()
    {
        $this->dropColumn('{{%video}}', 'source');
    }
}
