<?php

use yii\db\Migration;

/**
 * Class m180303_162004_add_table_news
 */
class m180303_162004_add_table_news extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->execute("
            CREATE TABLE `news` (
                `id` int(10) NOT NULL,
                `category_id` int(10) NOT NULL,
                `title_uz` text,
                `title_ru` text,
                `title_en` text,
                `title_cyrl` text,
                `description_uz` text,
                `description_ru` text,
                `description_en` text,
                `description_cyrl` text,
                `content_uz` text,
                `content_ru` text,
                `content_en` text,
                `content_cyrl` text,
                `images` varchar(255) DEFAULT NULL,
                `c_date` date NOT NULL,
                `viewed` int(11) NOT NULL,
                `slider` tinyint(1) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            --
            -- Indexes for table `news`
            --
            ALTER TABLE `news`
            ADD PRIMARY KEY (`id`);

        ");
    }

    public function down()
    {
        $this->dropTable('news');
    }
}
