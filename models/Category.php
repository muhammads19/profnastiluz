<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $category_type_id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_cyrl
 *
 * @property CategoryType $categoryType
 * @property Page[] $pages
 * @property Photo[] $photos
 * @property Video[] $videos
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'category_type_id'], 'integer'],
            [['category_type_id', 'name_uz', 'name_ru', 'name_en', 'name_cyrl'], 'required'],
            [['name_uz', 'name_ru', 'name_en', 'name_cyrl'], 'string', 'max' => 255],
            [['category_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryType::className(), 'targetAttribute' => ['category_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'category_type_id' => 'Category Type ID',
            'name_uz' => 'Name Uz',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'name_cyrl' => 'Name Cyrl',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryType()
    {
        return $this->hasOne(CategoryType::className(), ['id' => 'category_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(Video::className(), ['category_id' => 'id']);
    }

     public static function all($category_type = NULL, $not_id = NULL){
        $query = self::find();
        if($not_id){
            $res = $query->where(['!=', '{{%category}}.id', $not_id]);
        }
        if($category_type){
            if(is_numeric($category_type)){
                $query->andWhere(['{{%category}}.category_type_id'=>$category_type]);
            }else{
                $query->joinWith('categoryType')->andWhere("{{%category_type}}.name = '$category_type'");
            }
        }
        
        $res = $query->orderBy(['{{%category}}.name_uz'=>SORT_ASC])->asArray()->all();
        
        return \yii\helpers\ArrayHelper::map($res, 'id', 'name_uz');
    }
    
    public static function allParents($category_type = NULL, $not_id = NULL){
        
        $query = self::find()->where(['exists', (new \yii\db\Query())->select('children.id')->from('{{%category}} as children')->where('children.parent_id = {{%category}}.id')]);
        
        if($not_id){
            $res = $query->where(['!=', '{{%category}}.id', $not_id]);
        }
        if($category_type){
            if(is_numeric($category_type)){
                $query->andWhere(['{{%category}}.category_type_id'=>$category_type]);
            }else{
                $query->joinWith('categoryType')->andWhere("{{%category_type}}.name = '$category_type'");
            }
        }
        
        $res = $query->orderBy(['{{%category}}.name_uz'=>SORT_ASC])->asArray()->all();
        
        return \yii\helpers\ArrayHelper::map($res, 'id', 'name_uz');
    }

}
