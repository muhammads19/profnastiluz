<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_type".
 *
 * @property int $id
 * @property string $name
 *
 * @property Category[] $categories
 */
class CategoryType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['name'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['category_type_id' => 'id']);
    }

     public static function all(){
       return \yii\helpers\ArrayHelper::map(self::find()->orderBy(['id'=>SORT_ASC])->all(), 'id', 'name');
    }
}
