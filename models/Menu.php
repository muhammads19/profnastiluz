<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_cyrl
 * @property int $page_id
 * @property string $link
 * @property int $c_order
 * @property int $target_blank
 * @property int $status
 * @property int $visible_top
 * @property int $visible_side
 *
 * @property Menu $parent
 * @property Page $page
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'page_id', 'c_order', 'target_blank', 'status', 'visible_top', 'visible_side'], 'integer'],
            [['link'], 'string'],
            [['name_uz'], 'required'],
            [['name_uz', 'name_ru', 'name_en', 'name_cyrl'], 'string', 'max' => 255],
            [['parent_id', 'c_order'], 'default', 'skipOnEmpty' => false, 'value'=>0],
            [['visible_top', 'visible_side'], 'default', 'skipOnEmpty'=>false, 'value'=>1],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Qamrovchi menyu',
            'name_uz' => 'Menyu Nomi Uz',
            'name_ru' => 'Menyu Nomi Ru',
            'name_en' => 'Menyu Nomi En',
            'name_cyrl' => 'Menyu Nomi Kirill',
            'page_id' => 'Sahifa nomi',
            'link' => 'Havola (link)',
            'c_order' => 'Tartib',
            'target_blank' => 'Yangi oynada ochish',
            'status' => 'Aktiv',
            'visible_top' => 'Tepa menyuda ko\'rinishi',
            'visible_side' => 'Yon menyuda ko\'rinishi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getG355s()
    {
        return $this->hasMany(G355::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
    
    private $tmenus = [];
    private $byPMenus = [];

    /**
     * @return array
     */
    public function getMenusAsTree($parent_id = 0, $type = 'top')
    {
        $query = self::find()->where(['status'=>1]);
//        if($parent_id !== NULL){
//            $query->andWhere(['parent_id'=>$parent_id]);
//        }
        if($type == 'top'){
            $this->tmenus = $query->andWhere(['visible_top'=>1])->orderBy('parent_id, c_order, id')->indexBy('id')->asArray()->all();
        }elseif($type == 'side'){
            $this->tmenus = $query->andWhere(['visible_side'=>1])->orderBy('parent_id, c_order, id')->indexBy('id')->asArray()->all();
        }else{
            $this->tmenus = $query->orderBy('parent_id, c_order, id')->indexBy('id')->asArray()->all();
        }
       
        $this->byPMenus = [];
        foreach($this->tmenus as $item){
            if(!isset($this->byPMenus[$item['parent_id']])){
                $this->byPMenus[$item['parent_id']] = [];
            }
            $this->byPMenus[$item['parent_id']][] = $item['id'];
        }
        
        $result = [];
        
//        prd($this->tmenus);
        
        foreach($this->tmenus as $item){
            if($item['parent_id'] == $parent_id){
                $result[$item['id']] = $item;
                $result[$item['id']]['items'] = $this->_menuTree($item['id']);
            } elseif($item['parent_id'] > $parent_id) {
                break;
            }
        }
        
        $this->byPMenus = [];
        return $result;
    }
    
    private function _menuTree($mId){
        
        if( isset( $this->byPMenus[$mId] ) ){
            $result = [];
            foreach($this->byPMenus[$mId] as $item){
                $result[$item] = $this->tmenus[$item];
                $result[$item]['items'] = $this->_menuTree($item);
            }
            return $result;
        }else{
            return [];
        }
    }
    
    public static function allParents($not_id = NULL){
        $query = self::find()->where(['exists', (new \yii\db\Query())->select('menuchildren.id')->from('{{%menu}} as menuchildren')->where('menuchildren.parent_id = {{%menu}}.id')]);
        if($not_id){
            $res = $query->andWhere(['!=', 'id', $not_id]);
        }
        $res = $query->orderBy('parent_id, c_order, id')->asArray()->all();
        
        return \yii\helpers\ArrayHelper::map($res, 'id', 'name_uz');
    }
    
    public static function all($not_id = NULL){
        $query = self::find();
        if($not_id){
            $res = $query->where(['!=', 'id', $not_id]);
        }
        $res = $query->orderBy('parent_id, c_order, id')->asArray()->all();
        
        return \yii\helpers\ArrayHelper::map($res, 'id', 'name_uz');
    }
}
