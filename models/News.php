<?php

namespace app\models;
use dosamigos\taggable\Taggable;

use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title_uz
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_cyrl
 * @property string $description_uz
 * @property string $description_ru
 * @property string $description_en
 * @property string $description_cyrl
 * @property string $content_uz
 * @property string $content_ru
 * @property string $content_en
 * @property string $content_cyrl
 * @property integer $viewed
 * @property integer $slider
 *
 * @property Category $category
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $imagesFile;
    public $tagNames;
//    public $tags;
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['category_id','c_date'], 'required'],
            [['category_id','viewed', 'slider'], 'integer'],
            [['c_date'],'safe'],
            [['title_uz', 'title_ru', 'title_en', 'title_cyrl', 'description_uz', 'description_ru', 'description_en', 'description_cyrl', 'content_uz', 'content_ru', 'content_en', 'content_cyrl','imagesFile'], 'string'],
            [['slider', 'viewed'],'default', 'skipOnEmpty'=>false, 'value'=>0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Kategoriya',
            'title_uz' => 'Mavzu Lotin',
            'title_cyrl' => 'Mavzu Kirill',
            'title_ru' => 'Mavzu Ru',
            'title_en' => 'Mavzu En',
            'description_uz' => 'Qisqacha Lotin',
            'description_cyrl' => 'Qisqacha Kirill',
            'description_ru' => 'Qisqacha Ru',
            'description_en' => 'Qisqacha En',
            'content_uz' => 'Matn Lotin',
            'content_cyrl' => 'Matn Kirill',
            'content_ru' => 'Matn Ru',
            'content_en' => 'Matn En',
            'imagesFile' => 'Rasm',
            'c_date'=>'Sanasi',
            'slider'=>'Sliderga joylash',
            'viewed'=>'Ko`rishlar soni',
            'tagNames'=>'Teglar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public static function searchAnywhere($keyword) {
        if (!empty($keyword)) {
            $query = self::find();

            $keyword = trim($keyword);
            $keyword = htmlentities($keyword);
            
            $query->select('*');                  // выбираем только поле 'title'
            $query->orWhere(['like', 'title_uz', $keyword])
            ->orWhere(['like', 'title_ru', $keyword])
            ->orWhere(['like', 'title_en', $keyword])
            ->orWhere(['like', 'title_cyrl', $keyword])
            ->orWhere(['like', 'description_uz', $keyword])
            ->orWhere(['like', 'description_ru', $keyword])
            ->orWhere(['like', 'description_en', $keyword])
            ->orWhere(['like', 'description_cyrl', $keyword])
            ->orWhere(['like', 'content_uz', $keyword])
            ->orWhere(['like', 'content_ru', $keyword])
            ->orWhere(['like', 'content_en', $keyword])
            ->orWhere(['like', 'content_cyrl', $keyword]);

            $modelNews = $query->asArray()->all();
        } else {
            $modelNews = FALSE;
        }
        return $modelNews;
    }
//    public function beforeSave($insert) {
//        if (parent::beforeSave($insert)) {
//            if ($insert) {
//                
//            }
//            return true;
//        }
//        return false;
//    }

//    public function afterDelete() {
//        parent::afterDelete();
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour(){
        return $this->hasMany(Tour::className(), ['tour_id' => 'id']);
    }

    public function getByTag($tag_id){
        return $this::find()->innerJoin('{{%tour_tag_assn}}', '{{%news}}.id={{%tour_tag_assn}}.tour_id and {{%tour_tag_assn}}.tag_id = '.$tag_id)->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags() {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('{{%tour_tag_assn}}', ['tour_id' => 'id']);
    }
}
