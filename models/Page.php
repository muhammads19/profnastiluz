<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_cyrl
 * @property string $content_uz
 * @property string $content_ru
 * @property string $content_en
 * @property string $content_cyrl
 * @property string $description
 * @property string $created
 *
 * @property Menu[] $menus
 * @property Category $category
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_uz', 'content_uz'], 'required'],
            [['category_id'], 'integer'],
            [['name_uz', 'name_ru', 'name_en', 'name_cyrl', 'content_uz', 'content_ru', 'content_en', 'content_cyrl', 'description'], 'string'],
            [['created'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['category_id'], 'default', 'skipOnEmpty'=>false, 'value'=>NULL],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Kategoriya',
            'name_uz' => 'Sahifa Nomi Uz',
            'name_ru' => 'Sahifa Nomi Ru',
            'name_en' => 'Sahifa Nomi En',
            //'name_cyrl' => 'Sahifa Nomi Cyrl',
            'content_uz' => 'Sahifaga Matn Uz',
            'content_ru' => 'Sahifaga Matn Ru',
            'content_en' => 'Sahifaga Matn En',
            //'content_cyrl' => 'Sahifaga Matn Cyrl',
            'description' => 'Qisqacha bayoni',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)){
            if($insert){
                $this->created = date('Y-m-d H:i:s');
            }
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public static function all($category_id = NULL){
        
        $query = self::find();
        if($category_id){
            $res = $query->where(['category_id', $category_id]);
        }
        $res = $query->orderBy('name_uz')->asArray()->all();
        
        return \yii\helpers\ArrayHelper::map($res, 'id', 'name_uz');
    }

    public static function searchAnywhere($keyword) {
        if (!empty($keyword)) {
            $query = self::find();

            $keyword = trim($keyword);
            $keyword = htmlentities($keyword);
            
            $query->select('*');                  // выбираем только поле 'title'
            $query->orWhere(['like', 'name_uz', $keyword])
            ->orWhere(['like', 'name_ru', $keyword])
            ->orWhere(['like', 'name_en', $keyword])
            //->orWhere(['like', 'name_cyrl', $keyword])
            ->orWhere(['like', 'content_uz', $keyword])
            ->orWhere(['like', 'content_ru', $keyword])
            ->orWhere(['like', 'content_en', $keyword]);
            //->orWhere(['like', 'content_cyrl', $keyword]);
            
            $modelNews = $query->asArray()->all();
        } else {
            $modelNews = FALSE;
        }
        return $modelNews;
    }
}
