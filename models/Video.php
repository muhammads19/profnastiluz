<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property int $group
 * @property int $category_id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_cyrl
 * @property string $img
 * @property string $source
 * @property string $file
 *
 * @property Category $category
 */
class Video extends \yii\db\ActiveRecord {

    static $source = ['1' => 'Youtube', '2' => 'MyTube', '3' => 'uTube', '4' => 'Server'];
    public $videoFile;
    public $imgFile;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['group', 'category_id'], 'integer'],
            [['category_id', 'name_uz', 'file', 'source', 'file'], 'required'],
            [['name_uz', 'name_ru', 'name_en', 'name_cyrl', 'file', 'img'], 'string', 'max' => 255],
            [['source'], 'string', 'max' => 30],
            [['videoFile', 'imgFile'], 'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        
        return [
            'id' => 'ID',
            'group' => 'Group',
            'category_id' => 'Category ID',
            'name_uz' => 'Name Uz',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'name_cyrl' => 'Name Cyrl',
            'file' => 'File',
            'img' => 'Img',
            'source' => 'Source',
            'file' => 'File Name',
            'videoFile' => 'Video fayl',
            'imgFile' => 'Videoga rasm',
        ];
        
        return $parent;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function afterDelete() {
        parent::afterDelete();

        $path = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR;
        if ($this->file && $this->source == 4) {
            @unlink($path . $this->file);
        }
        if ($this->img) {
            @unlink($path . $this->img);
        }
    }   
    public static function searchAnywhere($keyword) {
        if (!empty($keyword)) {
            $query = self::find();

            $keyword = trim($keyword);
            $keyword = htmlentities($keyword);
            
            $query->select('*');                  // выбираем только поле 'title'
            $query->orWhere(['like', 'name_uz', $keyword])
            ->orWhere(['like', 'name_ru', $keyword])
            ->orWhere(['like', 'name_en', $keyword])
            //->orWhere(['like', 'name_cyrl', $keyword])
            ->orWhere(['like', 'file', $keyword])
            ->orWhere(['like', 'source', $keyword])
            ->orWhere(['like', 'fileName', $keyword]);
            //->orWhere(['like', 'content_cyrl', $keyword]);
            
            $modelNews = $query->asArray()->all();
        } else {
            $modelNews = FALSE;
        }
        return $modelNews;
    } 

}
