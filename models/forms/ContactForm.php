<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends \app\models\Contact
{
    /**
     * @var string verifyCode
     */
    public $verifyCode;

    /**
     * @return array the validation rules
     */
    public function rules(): array
    {
        return [
            [['full_name', 'email', 'phone', 'message'], 'required'],
            ['email', 'email'],
            [['full_name', 'email', 'phone'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels(): array
    {
        return [
            'full_name' => Yii::t('contact', 'Name'),
            'email' => Yii::t('contact', 'Email'),
            'subject' => Yii::t('contact', 'Subject'),
            'message' => Yii::t('contact', 'Body'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     *
     * @return bool whether the model passes validation
     */
    public function contact(string $email): bool
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->full_name])
                ->setSubject($this->phone)
                ->setTextBody($this->message)
                ->send();

            return true;
        }

        return false;
    }
}
