<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Menu;
use app\modules\admin\models\search\MenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $params = Yii::$app->request->queryParams;
        $params['MenuSearch']['parent_id'] = 0;
        
        $dataProvider = $searchModel->search($params);
        
        // validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable')) {
            // instantiate your book model for saving
            $pId = Yii::$app->request->post('editableKey');
            $model = Menu::findOne($pId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);

            // fetch the first entry in posted data (there should 
            // only be one entry anyway in this array for an 
            // editable submission)
            // - $posted is the posted data for Book without any indexes
            // - $post is the converted array for single model validation
            $post = [];
            $posted = current($_POST['Menu']);
            $post['Menu'] = $posted;

            // load model like any single model validation
            if ($model->load($post)) {
                // can save model or do something before saving model
                $model->save();

                // custom output to return to be displayed as the editable grid cell
                // data. Normally this is empty - whereby whatever value is edited by 
                // in the input by user is updated automatically.
                $output = '';

                // specific use case where you need to validate a specific
                // editable column posted when you have more than one 
                // EditableColumn in the grid view. We evaluate here a 
                // check to see if buy_amount was posted for the Book model
//                if (isset($posted['c_order'])) {
//                    $output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
//                }

                // similarly you can check if the name attribute was posted as well
                if (isset($posted['page_id'])) {
                    if($model->page_id){
                        $output = $model->page->name_uz; // process as you need
                    }  else {
                        $output = null; // process as you need
                    }
                }
                if (isset($posted['link'])) {
                    if($model->link){
                        $output = $model->link; // process as you need
                    }  else {
                        $output = null; // process as you need
                    }
                }
                if (isset($posted['status'])) {
                    $output = $model->status; // process as you need
                }
                if (isset($posted['target_blank'])) {
                    $output = $model->target_blank; // process as you need
                }
                if (isset($posted['visible_top'])) {
                    $output = $model->visible_top; // process as you need
                }
                if (isset($posted['visible_side'])) {
                    $output = $model->visible_side; // process as you need
                }
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            // return ajax json encoded response and exit
            echo $out;
            return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionDetail()
    {
        if(isset($_POST['editableKey'])){
            $parent_id = $_POST['editableKey'];
        }elseif($_POST['expandRowKey']){
            $parent_id = $_POST['expandRowKey'];
        }
        
        // validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable')) {
            // instantiate your book model for saving
            $pId = Yii::$app->request->post('editableKey');
            $model = Menu::findOne($pId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);

            // fetch the first entry in posted data (there should 
            // only be one entry anyway in this array for an 
            // editable submission)
            // - $posted is the posted data for Book without any indexes
            // - $post is the converted array for single model validation
            $post = [];
            $posted = current($_POST['Menu']);
            $post['Menu'] = $posted;

            // load model like any single model validation
            if ($model->load($post)) {
                // can save model or do something before saving model
                $model->save();

                // custom output to return to be displayed as the editable grid cell
                // data. Normally this is empty - whereby whatever value is edited by 
                // in the input by user is updated automatically.
                $output = '';

                // specific use case where you need to validate a specific
                // editable column posted when you have more than one 
                // EditableColumn in the grid view. We evaluate here a 
                // check to see if buy_amount was posted for the Book model
//                if (isset($posted['c_order'])) {
//                    $output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
//                }

                // similarly you can check if the name attribute was posted as well
                if (isset($posted['page_id'])) {
                    if($model->page_id){
                        $output = $model->page->name_uz; // process as you need
                    }  else {
                        $output = null; // process as you need
                    }
                }
                if (isset($posted['link'])) {
                    if($model->link){
                        $output = $model->link; // process as you need
                    }  else {
                        $output = null; // process as you need
                    }
                }
                if (isset($posted['status'])) {
                    $output = $model->status; // process as you need
                }
                if (isset($posted['target_blank'])) {
                    $output = $model->target_blank; // process as you need
                }
                if (isset($posted['visible_top'])) {
                    $output = $model->visible_top; // process as you need
                }
                if (isset($posted['visible_side'])) {
                    $output = $model->visible_side; // process as you need
                }
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            // return ajax json encoded response and exit
            echo $out;
            return;
        }
        
        $searchModel = new MenuSearch();
        
        $params = Yii::$app->request->queryParams;
        $params['MenuSearch']['parent_id'] = $parent_id;
        
        $dataProvider = $searchModel->search($params);

        return $this->renderAjax('detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parent_id' => $parent_id,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent_id = 0)
    {
        $model = new Menu();
        $model->status = 1;
        $model->visible_top = 1;
        $model->visible_side = 1;
        $model->parent_id = $parent_id;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
