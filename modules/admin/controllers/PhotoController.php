<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Photo;
use app\modules\admin\models\search\PhotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\imagine\Image;

/**
 * PhotoController implements the CRUD actions for Photo model.
 */
class PhotoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Photo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhotoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Photo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Photo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate() {
        $model = new Photo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $file = \yii\web\UploadedFile::getInstance($model, 'fileFile');
            $path = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR;
            if (!empty($file->name)) {
                $fileName = $model->id . "." . $file->extension;
                $file->saveAs($path . $fileName);
                
                Image::thumbnail(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR  . $fileName, 150, 100)
                    ->save(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR. $fileName, ['quality'=> 80]);

                $model->file = $fileName;
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Photo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   public function actionUpdate($id) {
        $model = $this->findModel($id);
        $oldfilename_img = $model->file;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $file_img_thumb = \yii\web\UploadedFile::getInstance($model, 'fileFile');
            $path = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR;
            if (!empty($file_img_thumb->name)) {
                $fileName_img = $model->id . "." . $file_img_thumb->extension;
                $file_img_thumb->saveAs($path . $fileName_img);
                
                Image::thumbnail(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR .$fileName_img, 200, 120)
                    ->save(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR. $fileName_img, ['quality'=> 80]);
                
                $model->file = $fileName_img;
                $model->save();

                if ($oldfilename_img != $fileName_img && $oldfilename_img) {
                    @unlink($path . $oldfilename_img);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Photo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Photo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
