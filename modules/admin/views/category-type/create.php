<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CategoryType */

$this->title = 'Kategoriya turlari';
$this->params['breadcrumbs'][] = ['label' => 'Kategoriya turlari', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-type-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
