<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contactlar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'full_name',
            'email:email',
            'phone',
            'message:ntext',
            //'created',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {delete}',
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>
</div>
