<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only"></span>
          </a>
          <!-- Navbar Right Menu -->
         
        <!-- search form -->
       
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget([
            'encodeLabels' => false,
            'items' => [
                // Important: you need to specify url as 'controller/action',
                // not just as 'controller' even if default action is used.
                // ['label' => 'Bosh sahifa', 'icon' => 'list', 'url' => ['/web/update?id=1']],
                ['label' => 'Menyu', 'icon' => 'list', 'url' => ['/admin/menu']],
                ['label' => 'Sahifa', 'icon' => 'pagelines', 'url' => ['/admin/page']],
                ['label' => 'Kategoriya', 'icon' => 'bars', 'url' => ['/admin/category']],
                ['label' => 'Kategoriya turlari', 'icon' => 'newspaper-o', 'url' => ['/admin/category-type']],
                ['label' => 'Yangiliklar', 'icon' => 'newspaper-o', 'url' => ['/admin/news']],
//                 ['label' => 'Savol-Javoblar', 'icon' => 'reply', 'items' => [
// //                        ['label' => 'Savol-javoblar', 'url' => ['/faq/index']],
//                         ['label' => 'Murojaatlar', 'url' => ['/site/contact']],
//                     ]],
//                ['label' => 'Xizmatlar', 'icon' => 'server', 'items' => [
//                        ['label' => 'Davlat ximatlari', 'url' => ['/gov-service/index']],
//                        ['label' => 'Interaktiv ximatlari', 'url' => ['/service/index']],
////                        ['label' => 'Axborot ximatlari', 'url' => ['#']],
//                        ['label' => 'Sifat tizimi sertifikatlashtirilgan<br>va davlat reestriga<br>kiritilgan korxonalar', 'url' => ['/reestr/reestr/index']],
//                        ['label' => 'Milliy standartlar katalogi', 'url' => ['/ns/ns/index']],
//                        ['label' => 'ISO standartlar katalogi', 'url' => ['/iso/iso/index']],
//                    ]],
//                ['label' => 'Xarita', 'icon' => 'map-marker', 'url' => ['/map/index']],
//                ['label' => 'Xalqaro hamkorlik', 'icon' => 'compress', 'url' => ['#']],
                ['label' => 'Galleriya', 'icon' => 'image', 'items' => [
                        ['label' => 'Foto galleriya', 'url' => ['/admin/photo']],
                        ['label' => 'Video galleriya', 'url' => ['/admin/video']],
                    ]],
                    ['label' => 'Contactlar', 'icon' => 'newspaper-o', 'url' => ['/admin/contact']],
//                 ['label' => 'Boshqalar', 'icon' => 'wrench', 'items' => [
//                         ['label' => 'Kalendar', 'url' => ['/calendar/index']],
//                         ['label' => 'Rahbariyat', 'url' => ['/personnal/index']],
//                         ['label' => 'Sayt xaritasi', 'url' => ['/sitemap/index']],
// //                        ['label' => 'Kitoblar', 'url' => ['/book/index']],
// //                        ['label' => 'Davlat Dasturlari', 'url' => ['/g-ser/index']],
//                     ]],
            ],
        ]);
                // 'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                // 'items' => [
                //     ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                //     ['label' => 'Menu', 'icon' => 'dashboard', 'url' => ['/admin/menu']],
                //     ['label' => 'category-type', 'icon' => 'dashboard', 'url' => ['/admin/category-type']],
                //     ['label' => 'Category', 'icon' => 'dashboard', 'url' => ['/admin/category']],
                    
                //     ['label' => 'Photo', 'icon' => 'dashboard', 'url' => ['/admin/photo']],
                //     ['label' => 'Page', 'icon' => 'dashboard', 'url' => ['/admin/page']],
                //     ['label' => 'Video', 'icon' => 'dashboard', 'url' => ['/admin/video']],

                //     ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    // [
                    //     'label' => 'Some tools',
                    //     'icon' => 'share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/admin'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                    //     ],
                    // ],
            //     ],
            // ]
        // )?>

    </section>

</aside>




 <?php
        // OR if this package is installed separately, you can use
      
           