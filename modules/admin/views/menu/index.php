<?php

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menyu';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->formatter->nullDisplay = '';
?>
<div class="menu-index">

   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?= Html::a('Menyu qo`shish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?= DynaGrid::widget([
        'options'=>['id'=>'dynagrid-menu'],
        'theme'=>'panel-info',
        'showPersonalize'=>true,
        'storage'=>DynaGrid::TYPE_COOKIE,
        'gridOptions'=>[
            'id'=>"grid-menu",
            'dataProvider'=>$dataProvider,
//            'filterModel'=>$searchModel,
            
            'tableOptions' => ['style'=>'margin-bottom:0px;'],
            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            
            'showPageSummary'=>false,
            'floatHeader'=>false,
            'pjax'=>false,
            'panel'=>[
                'type'=>GridView::TYPE_PRIMARY,
//                'heading'=>'<h3 class="panel-title">'.$this->title.'</h3>',
                'before' =>  '<div style="padding-top: 7px;"></div>',
                'after' => false
            ],
            'resizableColumns'=>false,
            'persistResize'=>false,
            'toolbar' =>  [
                ['content'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Qo\'shish', ['create'], ['type'=>'button', 'title'=>'Yangilik qo\'shish', 'class'=>'btn btn-success'])],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [NULL], ['data-pjax'=>1, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ],
            'exportConfig' => [
                GridView::EXCEL => [
                    'label' => 'Excel',
                    'icon' => 'floppy-remove',
                    'showHeader' => true,
                    'showPageSummary' => true,
                    'showFooter' => true,
                    'showCaption' => true,
                    'worksheet' => 'ExportWorksheet',
                    'filename' => 'Menyular',
                    'alertMsg' => 'The EXCEL export file will be generated for download.',
                    'cssFile' => '',
                    'options' => ['title' => 'Save as Excel']
                ],
            ],
        ],
        'options'=>['id'=>'dynagrid-0'],
        'columns' => [
//            ['class' => 'kartik\grid\SerialColumn'],

            [
                'class'=>'kartik\grid\ExpandRowColumn',
                'width'=>'50px',
                'value'=>function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl'=>\yii\helpers\Url::to(['/admin/menu/detail']),
//                'extraData'=> ['id'=>1],
                'detailOptions'=>['style'=>'margin-left:15px;'],
                'headerOptions'=>['class'=>'kartik-sheet-style'] ,
                'expandOneOnly'=>true
            ],
            [
                'attribute' => 'id',
                'hAlign' => 'middle',
                'vAlign' => 'middle',
            ],
            
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name_uz',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'placement' => PopoverX::ALIGN_RIGHT,

                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
           /* [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name_cyrl',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'placement' => PopoverX::ALIGN_RIGHT,
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],*/
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name_ru',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'placement' => PopoverX::ALIGN_RIGHT,
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name_en',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'placement' => PopoverX::ALIGN_RIGHT,
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'page_id',
                'value' => function($model){
                    return ($model->page_id ? $model->page->name_uz : NULL);
                },
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'header' => 'Sahifa nomi',
                    'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                    'options'=>[
                        'class'=>'form-control',
                        'data' => \app\models\Page::all(),
                        'pluginOptions' => [
                            'width'=>'400px',
                            'placeholder'=> "Select a State",
                            'allowClear'=>true,
                        ]
                    ],
                    'placement' => PopoverX::ALIGN_RIGHT,
                    'valueIfNull'=>'&nbsp;&nbsp;&nbsp;',
                ],
//                'options'=>['style'=>'width:400px;'],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'link',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'placement' => PopoverX::ALIGN_LEFT,
                    'valueIfNull'=>'&nbsp;&nbsp;&nbsp;',
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'c_order',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                    'options' => [
                        'pluginOptions' => ['min' => 0, 'max' => 5000]
                    ],
                    'placement' => PopoverX::ALIGN_LEFT,
                ],
                'hAlign' => 'right',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'target_blank',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
                    'displayValueConfig' => [
                        0 => '&nbsp;&nbsp;&nbsp;',
                        1 => '<span class="glyphicon glyphicon-ok text-success"></span>',
                    ],
                    'options' => [
                        'pluginOptions' => [
    //                            'handleWidth' => 60,
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>'
                        ]
                    ],
                    'placement' => PopoverX::ALIGN_LEFT,
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'status',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
                    'displayValueConfig' => [
                        0 => '<span class="glyphicon glyphicon-remove text-danger"></span>',
                        1 => '<span class="glyphicon glyphicon-ok text-success"></span>',
                    ],
                    'options' => [
                        'pluginOptions' => [
    //                            'handleWidth' => 60,
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>'
                        ]
                    ],
                    'placement' => PopoverX::ALIGN_LEFT,
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'visible_top',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
                    'displayValueConfig' => [
                        0 => '<span class="glyphicon glyphicon-remove text-danger"></span>',
                        1 => '<span class="glyphicon glyphicon-ok text-success"></span>',
                    ],
                    'options' => [
                        'pluginOptions' => [
    //                            'handleWidth' => 60,
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>'
                        ]
                    ],
                    'placement' => PopoverX::ALIGN_LEFT,
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'visible_side',
                'editableOptions' => [
                    'preHeader'=>'<i class="glyphicon glyphicon-edit"></i> ',
                    'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
                    'displayValueConfig' => [
                        0 => '<span class="glyphicon glyphicon-remove text-danger"></span>',
                        1 => '<span class="glyphicon glyphicon-ok text-success"></span>',
                    ],
                    'options' => [
                        'pluginOptions' => [
    //                            'handleWidth' => 60,
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>'
                        ]
                    ],
                    'placement' => PopoverX::ALIGN_LEFT,
                ],
                'hAlign' => 'middle',
                'vAlign' => 'middle',
                'refreshGrid' => true,
            ],

            ['class' => 'kartik\grid\ActionColumn', 'header'=>'Amallar'],
        ],
    ]); ?>

</div>
