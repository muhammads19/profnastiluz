<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = $model->name_uz;
$this->params['breadcrumbs'][] = ['label' => 'Menyu', 'url' => ['index']];
$tbreadcrumbs= [];
$a=0;
$tmodel = $model->parent;

  while($tmodel){
     
    $a++;
    
  $tbreadcrumbs[] = ['label' => $tmodel->name_uz, 'url' => ['view', 'id'=>
    $tmodel->id]];
    $tmodel = $tmodel->parent;


    if ($a > 15) break;   
}


for($i=count($tbreadcrumbs)-1;$i>=0;$i--){
    $this->params['breadcrumbs'][] = $tbreadcrumbs[$i];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-view">

   

    <p>
        <?= Html::a('O\'zgartirish', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('O\'chirish', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
//            'parent_id',
            'name_uz',
            'name_ru',
            'name_en',
            //'name_cyrl',
            [
                'attribute' => 'page_id',
                'value' => ($model->page_id ? $model->page->name_uz : ''),
            ],
            'link:ntext',
            'c_order',
            [
                'format' => 'raw',
                'attribute' => 'target_blank',
                'value' => ($model->target_blank ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>'),
            ],
            [
                'format' => 'raw',
                'attribute' => 'status',
                'value' => ($model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>'),
            ],
            [
                'format' => 'raw',
                'attribute' => 'visible_top',
                'value' => ($model->visible_top ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>'),
            ],
            [
                'format' => 'raw',
                'attribute' => 'visible_side',
                'value' => ($model->visible_side ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>'),
            ],
            [
                'attribute' => 'parent_id',
                'value' => ($model->parent_id ? $model->parent->name_uz : ''),
            ],
        ],
    ]) ?>

</div>
