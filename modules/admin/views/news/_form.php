<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\date\DatePicker;
use dosamigos\selectize;


/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'category_id')->dropDownList(\app\models\Category::all('news')) ?>

    <?= $form->field($model, 'title_uz')->textarea(['rows' => 2]) ?>

    <!--<? $form->field($model, 'title_cyrl')->textarea(['rows' => 2]) ?>-->

    <?= $form->field($model, 'title_ru')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'title_en')->textarea(['rows' => 2]) ?>
    
    <?= $form->field($model, 'description_uz')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>

    <?= $form->field($model, 'description_ru')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>
    
    <?= $form->field($model, 'description_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>

    <?= $form->field($model, 'content_uz')->widget(CKEditor::className(), [
        'options' => ['rows' => 3],
        'preset' => 'full'
    ]) ?>
    
    <?= $form->field($model, 'content_ru')->widget(CKEditor::className(), [
        'options' => ['rows' => 3],
        'preset' => 'full'
    ]) ?>
    
    <?= $form->field($model, 'content_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 3],
        'preset' => 'full'
    ]) ?>
    
    <?php
    echo $form->field($model, 'imagesFile')->widget(FileInput::classname(), [
    //    'options' => ['accept' => 'image/*'],
    ]);
    ?>
    
    <?php
    if($model->id && $model->images){
        echo "<img src='".Yii::getAlias('@web')."/files/photo/news/".$model->images."' style='width:400px;'>";
    }
    ?>
    <div style="padding-top: 10px;"></div>
    <?= $form->field($model, 'slider')->checkbox() ?>
    
    <?=
    $form->field($model, 'c_date')->widget(DatePicker::className(), [
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
            'autoclose' => true
        ]
    ])
    ?>
    
    <?= $form->field($model,'viewed')->textInput()?>

    <?= $form->field($model, 'tagNames')->widget(selectize\SelectizeTextInput::className(),[
        // calls an action that returns a JSON object with matched
        // tags
        'loadUrl' => ['tag/list'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => true,
        ],
    ])->hint('Teglarni tanlash uchun virgul bilan ajrating') ?>


   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Qo\'shish' : 'O\'zgartirish', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
