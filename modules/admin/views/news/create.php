<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'Yangilik qo`shish';
$this->params['breadcrumbs'][] = ['label' => 'Yangilik', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
