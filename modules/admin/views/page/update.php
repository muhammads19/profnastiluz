<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Page */

$this->title = 'Sahifa O`zgartirsh';
$this->params['breadcrumbs'][] = ['label' => 'Sahifa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'O`zgartirsh';
?>
<div class="page-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
