<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Page */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sahifa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <p>
        <?= Html::a('O`zgartirish', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('O`chirish', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'name_uz:ntext',
            'name_ru:ntext',
            'name_en:ntext',
            'name_cyrl:ntext',
            'content_uz:ntext',
            'content_ru:ntext',
            'content_en:ntext',
            'content_cyrl:ntext',
            'description:ntext',
            'created',
        ],
    ]) ?>

</div>
