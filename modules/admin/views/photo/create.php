<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Photo */

$this->title = 'Rasm Qo\'shish';
$this->params['breadcrumbs'][] = ['label' => 'Rasmlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
