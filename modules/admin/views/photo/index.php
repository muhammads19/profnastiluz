<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rasmlar';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile("@web/css/jquery-ui.css");
$this->registerJsFile(
    '@web/js/jquery-ui.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<div class="photo-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Qo\'shish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
          'id' => 'imageSort',
          'class' => 'table table-striped table-bordered'
        ],
        'columns' => [
            [ 'class' => 'yii\grid\SerialColumn' ],

           [
             'attribute' => 'id',
             'contentOptions' => ['class' => 'id'],
           ],
            [
                'attribute' => 'category_id',
                'value' => 'category.name_uz',
                'filter' => \app\models\Category::all('photo'),
            ],
            'name_uz',
            // 'name_ru',
            // 'name_en',
            // 'name_cyrl',
            // 'file',
            [
                'header' => 'Rasm',
                'format' => 'raw',
                'value' => function($model){
                    return Html::img(Yii::getAlias('@web').'/files/photo/thumb/'.$model->file, ['style'=>'width:150px; height:100px;']);
                }
            ],
            [
                'attribute' => 'chosed',
                'value' => function($model){
                    if ($model->chosed == 0) {
                      return 'Yoq';
                    }else{
                      return 'Xa';
                    }
                }
            ],
            'sorder',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php
$this->registerJs(
  "$('#imageSort tbody').addClass('sortable');
   $('#imageSort tbody tr').css('cursor', 'pointer');
   $('#imageSort tbody tr > td:first-child').addClass('numer');
   var url = '/site/sort'
   $('.sortable').sortable({
    stop: function( event, ui ) {

      var id = $('.sortable tbody td.id');
      var numer = $('.sortable tbody td.numer');

         $.ajax({
           method: 'POST',
           url: url,
           data: 'id=' + id
         })
         .success(function(data){
           console.log(data)
         })
         .fail(function(e){
           console.log(e)
         })
    }
   })
   ",
   $this::POS_READY
);

?>
