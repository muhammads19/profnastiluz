<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Photo */

$this->title = 'Rasmlarni o`zgartirish';
$this->params['breadcrumbs'][] = ['label' => 'Rasmlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'O`zgartirish';
?>
<div class="photo-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
