<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserModel */

$this->title = Yii::t('app', 'User O`zgartirsh', $model->username);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'O`zgartirsh');
?>
<div class="user-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
