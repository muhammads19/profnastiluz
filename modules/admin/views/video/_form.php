<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\Category;
use app\models\Video;

/* @var $this yii\web\View */
/* @var $model app\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'group')->textInput() ?>

    <?= $form->field($model, 'category_id')->dropDownList(Category::all('video')) ?>

    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_cyrl')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'file')->textInput(['maxlength' => true]) ?>
   



 
    <?= $form->field($model, 'source')->dropDownList(Video::$source, ['onchange'=>'hideShowByState()']) ?>
    
    <div class="file-area">
        <?= $form->field($model, 'file')->textarea(['rows' => 1]) ?>
    </div>
    
    <div class="video-file-chooser">
        <?php
        echo $form->field($model, 'videoFile')->widget(FileInput::classname(), [
            'options' => ['accept' => 'video/*'],
        ]);
        ?>
    </div>
    
    <div class="img-file-chooser">
        <?php
        echo $form->field($model, 'imgFile')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
        ]);
        ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
 

<script type="text/javascript">
    function hideShowByState(){
        var source = jQuery('#video-source').val();
        
        if(source == 4){
            jQuery('.video-file-chooser').show();
            jQuery('.img-file-chooser').show();
            jQuery('.file-area').hide();
        }else{
            jQuery('.video-file-chooser').hide();
            jQuery('.img-file-chooser').hide();
            jQuery('.file-area').show();
        }
    }
    <?php ob_start() ?>
        hideShowByState();
    <?php $this->registerJS(ob_get_clean()) ?>
</script>
