<?php
use yii\helpers\Url;
?>

<div id="get-quote" class="bg-color black text-center">
    <div class="container">
        <div class="row get-a-quote">
            <div class="col-md-12">
                <a class="black" href="<?= Url::to('site/contact') ?>">Биз билан боғланинг</a>
            </div>
        </div>
        <div class="move-top bg-color page-scroll">
            <a href="#page">
                <i class="glyphicon glyphicon-arrow-up"></i>
            </a>
        </div>
    </div>
</div>
<!-- request -->
<footer id="footer">
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                    <div class="widget-title">
                        <!-- Title -->
                        <h3 class="title">Манзил</h3>
                    </div>
                    <!-- Address -->
                    <p>
                        ООО «Musaffo Umid» <br />
                        Промзона ТХАЙ 1 <br>
                        Тошкент шахри
                    </p>
                    <a class="text-color" href="mailto:info@tom-baraka.uz">info@tom-baraka.uz</a>
                    <p>
                        +998 (98) 311-84-83 <br>
                        +998 (98) 309-71-09
                      </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 widget">
                        <div class="widget-title">
                            <!-- Title -->
                            <h3 class="title">Иш вақти</h3>
                        </div>
                        <nav>
                            <ul>
                                <!-- List Items -->
                                <li>
                                    <a href="#">9:00-18:00</a>
                                </li>
                                <li>
                                    <a href="#">Хар куни</a>
                                </li>
                            </ul>
                            <!-- Count -->
                            <div class="footer-count">
                                <p class="count-number" data-count="3550">total projects :
                                    <span class="counter"></span>
                                </p>
                            </div>
                            <div class="footer-count">
                                <p class="count-number" data-count="2550">happy clients :
                                    <span class="counter"></span>
                                </p>
                            </div>
                        </nav>
                    </div>
                <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                    <div class="widget-title">
                        <h3 class="title">Махсулотлар</h3>
                    </div>
                    <nav>
                        <ul>
                            <li>
                                <a href="<?= Url::to('site/profnastil') ?>">Профнастил</a>
                            </li>
                            <li>
                                <a href="<?= Url::to('tovar/tunukapon') ?>">Туникапон</a>
                            </li>
                            <li>
                                <a href="<?= Url::to('tovar/sendvich') ?>">Сендвич панеллар</a>
                            </li>
                            <li>
                                <a href="<?= Url::to('tovar/cherepitsa') ?>">Черепитца</a>
                            </li>
                            <li>
                                <a href="<?= Url::to('tovar/fasad') ?>">Фасадли копмозициялар</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 widget newsletter bottom-xs-pad-20">
                    <div class="widget-title">
                        <!-- Title -->
                        <h3 class="title">Axborot byulletenlarini ro'yxatdan o'tkazish:</h3>
                    </div>
                    <div>
                        <!-- Text -->
                        <p>Subscribe to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</p>
                        <p class="form-message1"></p>
                        <div class="clearfix"></div>
                        <!-- Form -->
                        <form id="subscribe_form" action="subscription.php" method="post" name="subscribe_form"
                              role="form">
                            <div class="input-text form-group has-feedback">
                                <input class="form-control" type="email" value="" name="subscribe_email" />
                                <button class="submit bg-color" type="submit">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </button></div>
                        </form>
                    </div>
                    <!-- Social Links -->

                </div>

                <!-- .newsletter -->
            </div>
        </div>
    </div>
    <!-- footer-top -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <!-- Copyrights -->
                <div class="col-xs-12 col-sm-6 col-md-6">&copy; "Musaffo Umid",  <?= date('Y') ?>
<!--                    <br />
                     Terms Link

                    <a href="#">Terms of Use</a> /
                    <a href="#">Privacy Policy</a>-->
                </div>
                <div class="col-xs-12 text-center visible-xs-block page-scroll gray-bg icons-circle i-3x">
                    <!-- Goto Top -->
                    <a href="#page">
                        <i class="glyphicon glyphicon-arrow-up"></i>
                    </a>
                </div>
                <div class="social-icon gray-bg icons-circle i-3x text-right">
                    <a href="https://t.me/tombaraka" target="_blank">
                        <i class="fa fa-telegram"></i>
                    </a>
                    <a href="https://instagram.com/tombarakauz" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://www.facebook.com/Tom-Baraka-380562856013157" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                  </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom -->
</footer>
<!-- footer -->
