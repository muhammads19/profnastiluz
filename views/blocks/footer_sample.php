<?php ?>

<div id="get-quote" class="bg-color black text-center">
    <div class="container">
        <div class="row get-a-quote">
            <div class="col-md-12">Get A Free Quote / Need a Help ? 
                <a class="black" href="#">Contact Us</a>
            </div>
        </div>
        <div class="move-top bg-color page-scroll">
            <a href="#page">
                <i class="glyphicon glyphicon-arrow-up"></i>
            </a>
        </div>
    </div>
</div>
<!-- request -->
<footer id="footer">
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                    <div class="widget-title">
                        <!-- Title -->
                        <h3 class="title">Address</h3>
                    </div>
                    <!-- Address -->
                    <p>
                        <strong>Office:</strong>ООО «Musaffo Umid»
                        <br />Пром.Зона ТХАЙ 1
                        <br />Тошкент шахри</p>
                    <!-- Email -->
                    <a class="text-color" href="mailto:info@zozothemes.com">info@tom-baraka.uz</a> 
                    <!-- Phone -->
                   <p>
                        <strong>Офис:</strong>ООО «Musaffo Umid»
                        <br />Пром.Зона ТХАЙ 1
                        <br />Тошкент шахри</p>
                    <!-- Email -->
                    <a class="text-color" href="mailto:info@tombaraka.uz">info@tombaraka.uz</a> 
                    <!-- Phone -->
                    <p></div>
                <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                    <div class="widget-title">
                        <!-- Title -->
                        <h3 class="title">Товарлар</h3>
                    </div>
                   <nav>
                        <ul>
                            <!-- List Items -->
                            <li>
                                <a href="#">Профнастил</a>
                            </li>
                            <li>
                                <a href="#">Туникапон</a>
                            </li>
                            <li>
                                <a href="#">Сендвич панеллар</a>
                            </li>
                            <li>
                                <a href="#">Черепитца</a>
                            </li>
                            <li>
                                <a href="#">Потолокли ва фасадли копмозициялар</a>
                            </li>
                            <li>
                                <a href="#">Панжара</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 widget">
                    <div class="widget-title">
                        <!-- Title -->
                        <h3 class="title">Business Hours</h3>
                    </div>
                    <nav>
                        <ul>
                            <!-- List Items -->
                            <li>
                                <a href="#">Monday-Friday: 9am to 5pm</a>
                            </li>
                            <li>
                                <a href="#">Saturday / Sunday: Closed</a>
                            </li>
                        </ul>
                        <!-- Count -->
                        <div class="footer-count">
                            <p class="count-number" data-count="3550">total projects : 
                                <span class="counter"></span>
                            </p>
                        </div>
                        <div class="footer-count">
                            <p class="count-number" data-count="2550">happy clients : 
                                <span class="counter"></span>
                            </p>
                        </div>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 widget newsletter bottom-xs-pad-20">
                    <div class="widget-title">
                        <!-- Title -->
                        <h3 class="title">Newsletter Signup</h3>
                    </div>
                    <div>
                        <!-- Text -->
                        <p>Subscribe to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</p>
                        <p class="form-message1"></p>
                        <div class="clearfix"></div>
                        <!-- Form -->
                        <form id="subscribe_form" action="subscription.php" method="post" name="subscribe_form"
                              role="form">
                            <div class="input-text form-group has-feedback">
                                <input class="form-control" type="email" value="" name="subscribe_email" /> 
                                <button class="submit bg-color" type="submit">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </button></div>
                        </form>
                    </div>
                    <!-- Social Links -->
                    <div class="social-icon gray-bg icons-circle i-3x">
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a> 
                        <a href="#">
                            <i class="fa fa-google"></i>
                        </a> 
                        </div>
                </div>
                <!-- .newsletter -->
            </div>
        </div>
    </div>
    <!-- footer-top -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <!-- Copyrights -->
                <div class="col-xs-12 col-sm-6 col-md-6">Copyright &copy; zozothemes.com., 2015
                    <br />
                    <!-- Terms Link -->

                    <a href="#">Terms of Use</a> / 
                    <a href="#">Privacy Policy</a></div>
                <div class="col-xs-12 text-center visible-xs-block page-scroll gray-bg icons-circle i-3x">
                    <!-- Goto Top -->
                    <a href="#page">
                        <i class="glyphicon glyphicon-arrow-up"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom -->
</footer>
<!-- footer -->