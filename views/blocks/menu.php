<?php

use yii\helpers\Url;

use app\models\Category;
 ?>
 <!--  Navbar -->
<div class="container">

 <!-- <?= yii\bootstrap\Html::dropDownList('category_id', null, Category::all(), ['class' => 'top-bar-section top-bar-bg-light shop-top-bar']) ?> -->
 </div>
<header class="new-version">
    <div class="container dark-header">
        <div class="row navbar navbar-default" role="navigation">
            <div class="col-md-12">
                <div class="navbar-header">
                    <!-- Button For Responsive toggle -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Navbar Collapse -->
                <div class="navbar-collapse collapse">
                    <!-- nav -->
                    <?=\app\components\Menu::widget();?>
                         <!--  <a class='languages' href="<?= \app\components\Helper::createMultilanguageReturnUrl('uz') ?>">Ўзб </a>  
                        <a class='languages' href="<?= \app\components\Helper::createMultilanguageReturnUrl('ru') ?>">Рус </a>
                        <a class='languages' href="<?= \app\components\Helper::createMultilanguageReturnUrl('en') ?>">Uzb</a>   -->


                     <ul class="navbar-nav">
                        <li class="top-social-icon icons-hover-black">
                            <a href="https://t.me/tombaraka" target="_blank">
                                <i class="fa fa-telegram"></i>
                            </a>
                            <a href="https://instagram.com/tombarakauz" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.facebook.com/Tom-Baraka-380562856013157" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
<!--                            <a href="#">-->
<!--                                <i class="fa fa-rss"></i>-->
<!--                            </a>-->
                         </li>

                        <!-- Header Search -->
                        <li class="hidden-767">
                            <a href="#" class="header-search">
                                <span>
                                    <i class="fa fa-search"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                     <ul class="nav navbar-nav">
                         <li class="top-parent">
                        <a href="#">Узб</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a  href="<?= \app\components\Helper::createMultilanguageReturnUrl('uz') ?>">
                                <img src="<?= Yii::getAlias('@web')?>/img/uz.jpg" alt="" />  Узб</a>
                            </li>
                            <li>
                                <a  href="<?= \app\components\Helper::createMultilanguageReturnUrl('ru') ?>">
                                <img src="<?= Yii::getAlias('@web')?>/img/ru.jpg" alt="" />  Ру</a>
                            </li>
                            <li>
                                <a  href="<?= \app\components\Helper::createMultilanguageReturnUrl('en') ?>">
                                <img src="<?= Yii::getAlias('@web')?>/img/uz.jpg" alt="" />  Uzb</a>
                            </li>
                        </ul>
                    </li>
                    </ul>
                    <!-- Right nav -->
                    <!-- Header Search Content -->
                    <div class="bg-white hide-show-content no-display header-search-content">
                    <form role="search" class="navbar-form vertically-absolute-middle" action="<?= Url::to(['/site/search']) ?>">
                            <div class="form-group">
                                <input type="text" placeholder="<?= Yii::t('template', 'Qidirish'); ?>" class="form-control" name="q" value="" />
                            </div>
                    </form>
                        <button class="close">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                    <!-- Header Search Content -->
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

</header>

<div id="top-bar" class="top-bar-section top-bar-bg-light shop-top-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <!-- Left nav -->

                <!-- Top Social Icon -->
            </div>
        </div>
    </div>
</div>