<?php

use app\components\Helper;// o`zgartirilganda o`zgarmadi
?>

<div class="new-version tb-pad-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-header">
                <!-- Button For Responsive toggle -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span></button>
                <!-- Logo -->


                <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>">
                    <img class="site_logo" alt="Site Logo" src="<?= Yii::getAlias('@web')?>/img/logo.png"/>

                </a></div>
                <!-- Navbar Collapse -->
                <div class="office-details">
                    <div class="detail-box">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <div class="detail">
                            <strong>
                              +998(98) 308-71-09<br>
                            </strong>
                            <strong>
                              +998(98) 309-71-09
                            </strong>
                            <!-- <a href="mailto:info@tom-baraka.uz">info@tom-baraka.uz</a></span>  -->
                        </div>
                    </div>
                    <div class="detail-box">
                        <div class="icon"><i class="fa fa-clock-o"></i></div>
                        <div class="detail">
                            <strong>Иш вакти</strong>
                            <strong>9:00-22:00</strong>
                        </div>
                    </div>
                    <div class="detail-box">
                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                        <div class="detail">
                            <strong>Промзона ТХАЙ 1</strong>
                            <strong>Тошкент шахар</strong>
                        </div>
                    </div>


                    <div class="col-sm-4 hidden-xs">
                      
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.col-md-12 -->
        </div>
    </div>
</div>
<!-- Top Bar --><!--
<div id="top-bar" class="top-bar-section top-bar-bg-light">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <!-- Top Contact --><!--
                <div class="top-contact link-hover-black">
                    <a href="#"><i class="fa fa-phone"></i>(98)311-84-83, (98)309-71-09</a>
                    <a href="#"><i class="fa fa-envelope"></i>info@tombaraka.com</a>
                </div>

                <!-- Top Social Icon --><!--
                <div class="top-social-icon icons-hover-black">
                    <a href="#">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-youtube"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-rss"></i>
                    </a>
                </div>


            </div>
        </div>
    </div>
</div> -->
