<section class="slider border-bottom line tp-banner-fullscreen-container">
    <div class="tp-banner">
        <ul>
            <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
                <div class="container">
                    <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="181" data-speed="1000"
                        data-start="1700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
                        <strong>Биз ишимизга</strong>
                    </h2>
                    <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="241" data-speed="1200"
                        data-start="1900" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
                        <strong>ижодий ёндошамиз</strong>
                    </h2>
                </div>
                <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide1.jpg" alt="" data-bgfit="cover" data-bgposition="center top"
                     data-bgrepeat="no-repeat" />
            </li>
            <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
                <div class="container">
                    <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="121" data-speed="1000"
                        data-start="1700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
                        <strong>Биз қурилиш-материаллари</strong>
                    </h2>
                    <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="181" data-speed="1200"
                        data-start="1900" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
                        <strong>ишлаб чиқариш</strong>
                    </h2>
                    <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="241" data-speed="1200"
                        data-start="1900" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
                        <strong>соҳасида етакчимиз</strong>
                    </h2>
                </div>
                <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide2.jpg" alt="" data-bgfit="cover" data-bgposition="center top"
                     data-bgrepeat="no-repeat" />
            </li>
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</section>
<!-- slider -->


<section id="features" class="page-section bottom-pad-0 transparent slider-block" data-animation="fadeInUp">
    <div class="container">
        <div class="row special-feature">
            <!-- Special Feature Box 1 -->
            <div class="col-md-3" data-animation="fadeInLeft">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-magic-wand"></i> 
                        <!-- Title -->
                        <h4>Тезкор</h4></div>
                    <div class="mask-bottom">
                        <!-- Icon -->
                        <i class="icon-magic-wand"></i> 
                        <!-- Title -->
                        <h4>Тезкор</h4>
                        <!-- Text -->
                        <p>Ҳар куни соат 8:00дан 20:00гача маҳсулотларимизни харид қилишингиз мумкин!</p></div>
                </div>
            </div>
            <!-- Special Feature Box 2 -->
            <div class="col-md-3" data-animation="fadeInUp">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-texture"></i> 
                        <!-- Title -->
                        <h4>Юқори технология</h4></div>
                    <div class="mask-bottom">
                        <!-- Icon -->
                        <i class="icon-texture"></i> 
                        <!-- Title -->
                        <h4>Юқори технология</h4>
                        <!-- Text -->
                        <p>Замонавий асбоб-ускуналарида ишлаб чиқарилган махсулотлар!</p></div>
                </div>
            </div>
            <!-- Special Feature Box 3 -->
            <div class="col-md-3" data-animation="fadeInRight">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-tree3"></i> 
                        <!-- Title -->
                        <h4>Хавфсиз</h4></div>
                    <div class="mask-bottom">
                        <!-- Icon -->
                        <i class="icon-tree3"></i> 
                        <!-- Title -->
                        <h4>Хавфсиз</h4>
                        <!-- Text -->
                        <p>Инсон саломатлигига мутлақо безарар маҳсулотлар.</p></div>
                </div>
            </div>
            <!-- Special Feature Box 3 -->
            <div class="col-md-3" data-animation="fadeInUp">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-group-outline"></i> 
                        <!-- Title -->
                        <h4>Ҳамён Боп</h4></div>
                    <div class="mask-bottom">
                        <!-- Icon -->
                        <i class=" icon-group-outline"></i> 
                        <!-- Title -->
                        <h4>Ҳамён Боп</h4>
                        <!-- Text -->
                        <p>Маҳсулотларни савдолашиб танлов асосида сотиб олишингиз мумкин.</p></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- features -->