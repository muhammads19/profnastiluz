<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Богланиш';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Sticky Navbar -->
<div class="page-header page-title-left page-title-pattern">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="title">БОҒЛАНИШ</h1>
        <h5>Биз билан боғланинг</h5>
        <ul class="breadcrumb">
          <li>
            <a href="/">Бош сахифа</a>
          </li>
          <li class="active">Боғланиш</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- page-header -->
<section id="contact-us" class="page-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6">
        <div class="row">
          <div class="col-sm-6 col-md-6">
            <h5 class="title">
              <i class="icon-address text-color"></i>
              Бизнинг Манзилимиз
            </h5>
            <address>
              Тошкент Шаҳри <br />
              Пром-зона ТХАЙ 1 <br />
              Индекс 100001.
            </address>
          </div>
          <div class="col-sm-6 col-md-6">
            <h5 class="title">
              <i class="icon-contacts text-color"></i>Боғланиш маълумотлари </h5>
              <div>+998(98) 308-71-09</div>
              <div>+998(98) 309-71-09</div>
              <div>+998(98) 311-84-83</div>
              <div> <a href="mailto:info@tom-baraka.uz">info@tom-baraka.uz</a></div>
            </div>
          </div>
          <hr />
          <p> Қисқача Профнастил ҳақида ма`лумотлар </p>
          <p>Ишлаб чиқариш ҳақида ма`лумотлар </p>
          <p> Келажакдаги ҳаракатларимиз,режаларимиз ва юқори даражадаги технологиялар билан жиҳозланган кенг ўлчамдаги корхона барпо этиш </p>
        </div>
        <div class="col-md-6 col-md-6">
          <h3 class="title">Боғланиш</h3>
          <p class="form-message"></p>
          <div class="contact-form">
            <!-- Form Begins -->
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <?=$form->errorSummary($model); ?>

            <form role="form" name="contact" id="contact" method="post">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-text form-group">
                    <?= $form->field($model, 'full_name')->textInput()->label('Исм ') ?>
                    <!--  <input type="text" name="contact_name" class="input-name form-control"
                    placeholder="Full Name" /> -->
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-email form-group">
                    <?= $form->field($model, 'email')->input('email')->label('Почта') ?>
                    <!-- <input type="email" name="contact_email" class="input-email form-control"
                    placeholder="Email" /> -->
                  </div>
                </div>
              </div>
              <div class="input-email form-group">
                <?= $form->field($model, 'phone')->label('Тел номер') ?>
                <!-- <input type="text" name="contact_phone" class="input-phone form-control" placeholder="Phone" /> -->
              </div>
              <div class="textarea-message form-group">
                <?= $form->field($model, 'message')->textarea()->label('Хабар'); ?>
                <!-- <textarea name="contact_message" class="textarea-message form-control" placeholder="Message"
                rows="6"></textarea>
              --> </div>
              <div>

                <?= Html::submitButton('Юбориш <i class="icon-paper-plane"></i>', ['class' => 'btn btn-default','name' => 'contact-button']) ?></div>
              </form>
              <?php ActiveForm::end(); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- page-section -->
    <section id="map" style="max-height:400px">
        <iframe src="https://yandex.com/map-widget/v1/?um=constructor%3A1fadc70d4cd3ffa142d7f98c8f84243533de27c549265caad6dba6e59f2a9b7e&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
    </section>
    <!-- map -->
