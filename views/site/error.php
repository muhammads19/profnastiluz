<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    

     <div class="page-header page-title-left page-title-pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title"><h1><?= Html::encode($this->title); ?></h1></h1>
                        <h5>A Short Page title</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- page-header -->
        <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h2 class="upper"><?= nl2br(Html::encode($message)); ?></h2>
                        <p>Try searching for the best match or browse the links below:</p>
                    </div>
                </div>
            </div>
        </section>
      
      
</div>
