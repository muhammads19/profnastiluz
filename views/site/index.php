<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\JWPlayerAsset;
use yii\helpers\Url;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */
$this->title = 'Бош сахифа';
?>

<?php require Yii::getAlias('@app').'/views/site/_slider.php'; ?>

<section id="services" class="page-section transparent">
  <div class="container">
    <div class="row">
      <div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="3"
      data-autoplay="true" data-navigation="true">

      <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInUp">

        <p class="text-center">
          <a href="<?= Yii::getAlias('@web') ?>/img/sections/services/17.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">

            <img src="<?= Yii::getAlias('@web') ?>/img/sections/services/17.jpg" width="420" height="280" alt="" />
          </a>
        </p>

        <h3>
          <a href="#">Профнастил 17 гофра</a>
        </h3>
        <div class="eqh">
          <p>Ички қатламли темир бинолар,том, девор,темир карказли конструкциялар ,ерли участкалар,омборхоналар,гаражлар  учун қулай чиройли профиллист ёки универсал профнастил.</p>
        </div>
        <a href="<?= Url::to('site/profnastil') ?>" class="btn btn-default">Батафсил</a>
      </div>
      <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInUp">

        <p class="text-center">
          <a href="<?= Yii::getAlias('@web') ?>/img/sections/services/22.1
            .jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
            <img src="<?= Yii::getAlias('@web') ?>/img/sections/services/22.1.jpg" width="420" height="280" alt="" />
          </a>
        </p>

        <h3>
          <a href="#">Профнастил 22 гофра</a>
        </h3>
        <div class="eqh">
          <p>Ички қатламли темир бинолар,том, девор,темир карказли конструкциялар ,ерли участкалар,омборхоналар,гаражлар  учун қулай чиройли профиллист ёки универсал профнастил.</p></div>
          <a href="<?= Url::to('site/profnastil') ?>" class="btn btn-default">Батафсил</a>
        </div>
        <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInUp">
          <p class="text-center">
            <a href="<?= Yii::getAlias('@web') ?>/img/sections/services/35.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
              <img src="<?= Yii::getAlias('@web') ?>/img/sections/services/35.jpg" width="420" height="280" alt="" />
            </a>
          </p>

          <h3>
            <a href="#">Профнастил 35 гофра</a>
          </h3>
          <div class="eqh">
            <p>Ички қатламли темир бинолар,том, девор,темир карказли конструкциялар ,ерли участкалар,омборхоналар,гаражлар  учун қулай чиройли профиллист ёки универсал профнастил.</p></div>
            <a href="<?= Url::to('site/profnastil') ?>" class="btn btn-default">Батафсил</a>
          </div>
          <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInUp">

            <p class="text-center">
              <a href="<?= Yii::getAlias('@web') ?>/img/sections/services/57.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                <img src="<?= Yii::getAlias('@web') ?>/img/sections/services/57.jpg" width="420" height="280" alt="" />
              </a>
            </p>

            <h3>
              <a href="#">Профнастил 57 гофра</a>
            </h3>
            <div class="eqh">
              <p>Ички қатламли темир бинолар,том, девор,темир карказли конструкциялар ,ерли участкалар,омборхоналар,гаражлар  учун қулай чиройли профиллист ёки универсал профнастил. </p></div>
              <a href="<?= Url::to('site/profnastil') ?>" class="btn btn-default">Батафсил</a>
            </div>
            <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInLeft">

              <p class="text-center">
                <a href="<?= Yii::getAlias('@web') ?>/img/sections/services/11.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                  <img src="<?= Yii::getAlias('@web') ?>/img/sections/services/11.jpg" width="420" height="280" alt="" />
                </a>
              </p>

              <h3>
                <a href="#">Туникапон</a>
              </h3>
              <div class="eqh">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
                  deserunt a enim harum eaque fugit.</p></div>
                  <a href="<?= Url::to('tovar/tunukapon') ?>" class="btn btn-default">Батафсил</a>
                </div>



                <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInRight">

                  <p class="text-center">
                    <a href="<?= Yii::getAlias('@web') ?>/img/sections/services/02.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                      <img src="<?= Yii::getAlias('@web') ?>/img/sections/services/02.jpg" width="420" height="280" alt="" />
                    </a>
                  </p>

                  <h3>
                    <a href="#">Сендвич панеллар</a>
                  </h3>
                  <div class="eqh">
                    <p>

                      Панеллар компонентлари сифатида бир-бири билан елимланган металлдан қилинган варақлар ва иситкич хизмат қилади. Иситкич- турли зичликдаги пенополистирол бўлади.
                    </p></div>
                    <a href="<?= Url::to('tovar/sendvich') ?>" class="btn btn-default">Батафсил</a>
                  </div>
                  <div class="col-sm-4 col-md-4 col-xs-12">

                    <p class="text-center">
                      <a href="<?= Yii::getAlias('@web') ?>/img/sections/services/14.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                        <img src="<?= Yii::getAlias('@web') ?>/img/sections/services/14.jpg" width="420" height="280" alt="" />
                      </a>
                    </p>

                    <h3>
                      <a href="#">Черепитца</a>
                    </h3>
                    <div class="eqh">
                      <p>Черепица  — асосан, бино томига ёпиш учун ишлатиладиган курилиш материали.Ўзбекистонда кам тарқалган, лекин кейинги йилларда пластмасса, металл ва бошқа материаллардан тайёрланган Ч.лар пайдо бўла бошлади.</p></div>
                      <a href="<?= Url::to('tovar/cherepitsa') ?>" class="btn btn-default">Батафсил</a>
                    </div>

                  </div>


                </div>
              </div>
            </section>

            <script type="text/javascript">
            <?php ob_start() ?>
            $('.eqh').matchHeight();
            <?php $this->registerJs(ob_get_clean()) ?>
            </script>
            <!-- Services -->

            <section id="who-we-are" class="page-section light-bg border-tb">
              <div class="container who-we-are">
                <div class="section-title text-left">
                  <!-- Title -->
                  <h2 class="title"><?= Yii::t('template', 'Biz haqimizda') ?></h2>
                </div>
                <div class="row">
                  <!-- <div class="col-md-4">
                    <img src="<?= Yii::getAlias('@web') ?>/img/sections/about/founder.jpg" alt="" width="400" height="300" />
                    <p class="description upper">«Мусаффо Умид» МЧЖ</p>
                    <p>"Мусаффо Умид" МЧЖ тезкор ривожланаётган компания бўлиб, 1998 йилда ўз фаолиятини бошлади. Асосий фаолият - юқори сифатли пўлат маҳсулотларни ишлаб чиқариш ва сотишдир. «Мусаффо Умид» МЧЖ уйингизда ва деворлар, металл уйингизда, металл девор ва том сендвич панеллар, тарнов тизими, шунингдек, уйингизда ва ўзининг учун барча аксессуарлари ишлаб чиқаради ва сотади. Темир пўлатдан қалинлиги 3 мм қалинликдаги темир конструкциялар.</p>
                  </div> -->
                  <div class="col-md-4">
                    <div class="item-box bottom-pad-10">
                      <a>
                        <i class="icon-star13 i-5x bg-color"></i>
                        <h4>«Мусаффо Умид» МЧЖ</h4>
                        <!-- <p>Ҳар қандай компанияда ходимлар муҳим рол ўйнайди. Биз вазалар, картон, унинг қалинлиги ва юк салоҳиятини трапезоидал варақ уйингизда ва деворлар, металл уйингизда, металл жабҳа, девор ва том сендвич панеллар, тарнов тизими ёки брендини танлашда ҳар қандай ёрдам ва бепул маслаҳат бериш учун тайёр профессионаллар жамоаси тўпланди. </p>-->
                        <p>«Мусаффо Умид» МЧЖ уйингизда ва деворлар, металл уйингизда, металл девор ва том сендвич панеллар, тарнов тизими, шунингдек, уйингизда ва ўзининг учун барча аксессуарлари ишлаб чиқаради ва сотади том сендвич панеллар, тарнов тизими, шунингдек, учун барча аксессуарлари ишлаб чиқаради ва сотади.</p>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item-box bottom-pad-10">
                      <a>
                        <i class="icon-heart18 i-5x bg-color"></i>
                        <h4>Нега мижозлар бизни маъқул кўради?</h4>
                        <p>Улар бизга ишонишади, биз билан ишлашади ва бизни танлашади . Биз юқори сифатли,
                          Россия ва Хитойда ишлаб чиқарилган энг олди  ускуналарда полмер билан қопланган ва ёки галванизланган темир профнастил ва бошқа маҳсулотларни таклиф этамиз!</p>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="item-box bottom-pad-10">
                        <a>
                          <i class="icon-gift6 i-5x bg-color"></i>
                          <h4>Биз нима таклиф қиламиз?</h4>
                          <p>Савдо, логистика ишини яхши ташкил этилган ва ишлаб чиқариш бизга буюртмачи томонидан белгиланган муддатлари дош куни фармойишларига, тўлиқ машиналари ва тегишли материаллар амалга ошириш имконини беради. Биз мижозимизга ва унинг вақтини қадрлаймиз.    </p>
                        </a>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </section>
            <!-- who-we-are -->

            <section id="works" class="page-section transparent">
              <!--<div class="work-section">-->
              <!--                <div class="section-title">
              Heading
              <h2 class="title">Галерея</h2>
            </div>-->
            <div class="container">
              <div class="section-title text-left">
                <!-- Heading -->
                <h2 class="title"><?= Yii::t('template', 'Галерея') ?></h2>
              </div>
            </div>

            <?= \app\components\Gallery::widget(); ?>
            <!--</div>-->
          </section>
          <!-- works -->

          <!--        <section id="team" class="page-section light-bg border-tb">
          <div class="container text-center">
          <div class="section-title">
          <h2 class="title">Meet the Team</h2>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="owl-carousel pagination-1 dark-switch text-center" data-pagination="true" data-autoplay="true"
        data-navigation="false" data-items="3">
        <div class="item">
        <div class="client-image">
        Image
        <img class="img-circle" src="img/sections/team/1.jpg" width="80" height="80" alt="" />
      </div>
      <div class="client-details text-center">
      <p>The standard chunk of Lorem Ipsum used since the 1500s is
      <br />reproduced below for those intereste.</p>
      <div class="client-details">
      Name
      <strong class="text-color">Phillip Parisis</strong>
      Company

      <span>Project Manager</span></div>
    </div>
  </div>
  <div class="item">
  <div class="client-image">
  Image
  <img class="img-circle" src="img/sections/team/2.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<p>The standard chunk of Lorem Ipsum used since the 1500s is
<br />reproduced below for those intereste.</p>
<div class="client-details">
Name
<strong class="text-color">Simo Kruyt</strong>
Company

<span>Construction Manager</span></div>
</div>
</div>
<div class="item">
<div class="client-image">
Image
<img class="img-circle" src="img/sections/team/3.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<p>The standard chunk of Lorem Ipsum used since the 1500s is
<br />reproduced below for those intereste.</p>
<div class="client-details">
Name
<strong class="text-color">Jorge Canaveral</strong>
Company

<span>Architect</span></div>
</div>
</div>
<div class="item">
<div class="client-image">
Image
<img class="img-circle" src="img/sections/team/4.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<p>The standard chunk of Lorem Ipsum used since the 1500s is
<br />reproduced below for those intereste.</p>
<div class="client-details">
Name
<strong class="text-color">Aimee Devlin</strong>
Company

<span>Sales Manager</span></div>
</div>
</div>
<div class="item">
<div class="client-image">
Image
<img class="img-circle" src="img/sections/team/1.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<p>The standard chunk of Lorem Ipsum used since the 1500s is
<br />reproduced below for those intereste.</p>
<div class="client-details">
Name
<strong class="text-color">John Doe</strong>
Company

<span>Resource Head</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>-->
<!-- team -->


<section id="latest-news" class="page-section light-bg border-tb">
  <div class="container">
    <div class="section-title text-left">
      <!-- Heading -->
      <h2 class="title">Видео галерея</h2>
    </div>
  </div>

  <?= \app\components\Video::widget(); ?>

</section>

<?php /*

<!-- fun-factor -->
<!-- <section id="latest-news" class="page-section transparent border-tb">
<div class="container">
<div class="section-title">
<h2 class="title">Yangiliklar</h2>
</div>
<div class="row">
<div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="3"
data-autoplay="true" data-navigation="true">

<?php //\app\components\News::widget(); ?>

</div>
</div>
</div>
</section> -->
<!--        <section id="testimonials" class="page-section">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12 testimonials">
<div class="owl-carousel pagination-1 dark-switch text-center" data-pagination="true" data-autoplay="true"
data-navigation="false" data-items="3">
<div class="item col-md-4 col-sm-6">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim ducimus eveniet distinctio amet
quaerat maxime fugit nesciunt sunt ut quasi nulla.</p>
<div class="star-rating text-center">
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star-half-o text-color"></i></div>
<div class="client-image">
Image
<img class="img-circle" src="img/sections/testimonials/1.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<div class="client-details">
Name
<strong class="text-color">John Doe</strong>
Company

<span>CEO</span></div>
</div>
</div>
<div class="item col-md-4 col-sm-6">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim ducimus eveniet distinctio amet
quaerat maxime fugit nesciunt sunt ut quasi nulla.</p>
<div class="star-rating text-center">
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star-half-o text-color"></i></div>
<div class="client-image">
Image
<img class="img-circle" src="img/sections/testimonials/2.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<div class="client-details">
Name
<strong class="text-color">John Doe</strong>
Company

<span>CEO</span></div>
</div>
</div>
<div class="item col-md-4 col-sm-6">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim ducimus eveniet distinctio amet
quaerat maxime fugit nesciunt sunt ut quasi nulla.</p>
<div class="star-rating text-center">
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star-half-o text-color"></i></div>
<div class="client-image">
Image
<img class="img-circle" src="img/sections/testimonials/3.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<div class="client-details">
Name
<strong class="text-color">John Doe</strong>
Company

<span>CEO</span></div>
</div>
</div>
<div class="item col-md-4 col-sm-6">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim ducimus eveniet distinctio amet
quaerat maxime fugit nesciunt sunt ut quasi nulla.</p>
<div class="star-rating text-center">
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star-half-o text-color"></i></div>
<div class="client-image">
Image
<img class="img-circle" src="img/sections/testimonials/1.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<div class="client-details">
Name
<strong class="text-color">John Doe</strong>
Company

<span>CEO</span></div>
</div>
</div>
<div class="item col-md-4 col-sm-6">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim ducimus eveniet distinctio amet
quaerat maxime fugit nesciunt sunt ut quasi nulla.</p>
<div class="star-rating text-center">
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star text-color"></i>
<i class="fa fa-star-half-o text-color"></i></div>
<div class="client-image">
Image
<img class="img-circle" src="img/sections/testimonials/3.jpg" width="80" height="80" alt="" />
</div>
<div class="client-details text-center">
<div class="client-details">
Name
<strong class="text-color">John Doe</strong>
Company

<span>CEO</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>-->
<!-- testimonials -->

<!--
<section id="contact-us" class="page-section">
<div class="container">
<div class="row">
<div class="col-md-4 opacity">
<h3>We are Go Green!</h3>
<img src="img/sections/about/go-green.jpg" width="500" height="250" alt="" />
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste exercitationem
praesentium deleniti nostrum laborum rem id nihil tempora.</p>
</div>
<div class="col-md-4">
<p class="form-message"></p>
<div class="contact-form" >

<?php $form// = ActiveForm::begin(['action' =>['create'], 'id' => 'id', 'method' => 'post']); ?>

<?//= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

<?//= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?//= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

<?//= $form->field($model, 'message')->textarea(['rows' => 1.5]) ?>

<div class="form-group">
<?//= Html::submitButton('Junatish' ,['class'=>'btn btn-default btn-block']) ?>
</div>

<?php// ActiveForm::end(); ?>

</div>
</div>
<div class="col-md-4">
<div class="map-section">
<div class="map-canvas" data-zoom="12" data-lat="-35.2835" data-lng="149.128" data-type="roadmap"
data-title="Austin"
data-content="Company Name&lt;br&gt; Contact: +012 (345) 6789&lt;br&gt; &lt;a href=&#39;mailto:info@youremail.com&#39;&gt;info@youremail.com&lt;/a&gt;"
style="height: 350px;"></div>
</div>
</div>
<!- map /-/->
</div>
</div>
</section> -->

<!-- <div class="site-index">
<div class="jumbotron">
<h1>Congratulations!</h1>
<p class="lead">You have successfully created your Yii-powered application.</p>
</div>
</div>
-->

*/?>
