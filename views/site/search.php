<?php
/* @var $this SiteController */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = Yii::t('template', 'Qidiruv');
$this->params['breadcrumbs'][] = $this->title;
?>
 <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h2 class="upper"><?= $this->title; ?></h2>
                        <form action="<?= Url::to(['/site/search']) ?>" method ="get">
                            <div class="input-group input-group-lg">
                           <input type='text' name='q' placeholder="<?= Yii::t('template', 'Qidiruv'); ?>" class="form-control" value="<?= @$_GET['q']; ?>">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default">
                                    <span class="input-group-btn">
                                        <span><?= Yii::t('template', 'Search') ?></span>
                                    </span>
                                </button>
                            </span></div>
            <div id="advanced-search" style="display: none; margin-bottom: 10px; width: 300px;">
                <?= Html::dropDownList('section', (isset($_GET['section']) ? $_GET['section'] : NULL), [
                        NULL => Yii::t('template', 'Hamma joydan'),
                        'photo' => Yii::t('template', 'Rasimlar'),
                        'page' => Yii::t('template', 'Sahifalar'),
                        'video' => Yii::t('template', 'Videolar'),
                        'contact' => Yii::t('template', 'Contactlar'),
                    ],
                    ['class' => 'form-control']
                )
                ?>
            </div>

                        </form>
                    </div>
                </div>
            </div>

       </section>

<style>
    p, pre, ul, ol, dl, dd, blockquote, address, table, fieldset, form {
        margin-bottom: 3px;
    }
</style>
<div class="container">
<div class="row">
<div style="margin-top: 0px;">
</div>
</div>
    <?php
    // search kodi bor funksiya 
    $i = 0;
    $stop = false;
    foreach ($result as $sectionKey => $sectionValue) {
        if ($stop) {
            break;
        }
        if (is_array($sectionValue)) {
            foreach ($sectionValue as $item) {
                if ($sectionKey == 'news') {
                    $link = Html::a($item['title_uz'], ['/news/view', 'id' => $item['id']]);
                    $content = strip_tags($item['description_uz']);
                } elseif ($sectionKey == 'page') {
                    $link = Html::a(strip_tags($item['name_uz']), ['/page/view', 'id' => $item['id']]);
                    $content = substr(strip_tags($item['content_uz']), 0, 600) . "...";
                }elseif('faq' == 'faq'){
                    $link = '';
                    $content = (trim($item['answer_uz']) ? $item['answer_uz'] : $item['question_uz']);
                    if(!$content)
                        continue;
                }else{
                    $link = '';
                    $content = (isset($item['content_uz']) ? $item['content_uz'] : $sectionKey);
                }

                echo "<div>
                    <p>$link</p>
                    <div>
                    {$content}
                    </div>
                    </div><hr>";

                $i++;
                if ($i == 20) {
                    $stop = TRUE;
                    break;
                }
            }
        }
    }
    ?>

</div>