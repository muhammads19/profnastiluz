 <div id="page" class="page-wrap">
        <!-- Page Loader -->
        <div id="pageloader">
            <div class="loader-item fa fa-spin text-color"></div>
        </div>
        		<section id="welcome" class="page-section new-dark">
            <div class="container">
                <div class="row">
					<div class="col-md-12 text-center white">
						<h2 class="heading" data-animation="fadeInUp"> It Is Ultimate Flexible For <span class="text-color">Construction</span> Company</h2>
						<p data-animation="fadeInUp"> Clean and simple with loads of nice options and features. It’s 100% responsive design was tested on all major handheld devices. It includes more than 50 pages to make your website stand out from the crowd. Unlimited color options give you great look and feel. Unlimited features added in a single theme. Don’t wait – launch your website now!</p>
						<br />
						<br />
						<div  data-animation="pulse">
							<img src="img/sections/slider/tab-screen.png" width="1164" height="590" alt="" />
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- welcome -->
		<section id="action" class="page-section tb-pad-20 bg-color">
            <div class="container">
				<div class="row">
					<div class="col-md-12 top-pad-10 white" data-animation="fadeInUp">
						<h2 class="heading">You can choose from our creative Home page layouts to create your own awesome Website. <span class="black">Clean and simple </span>code and UI elements helps superb customizable!!</h2>
					</div>
				</div>
			</div>
        </section>
        <!-- call to action -->
        <section id="services" class="page-section">
			<div class="image-bg content-in fixed" data-background="img/sections/bg/intro.jpg"></div>
            <div class="container">
				<div class="row">
                    <div class="col-md-12 bottom-pad-30 text-center white" data-animation="fadeInUp">
                        <!-- Title -->
                        <h2 class="heading">We are <span class="text-color">Experts</span></h2>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-40" data-animation="fadeInLeft">
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/1.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/1.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-1.html">General Contracting</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
								deserunt a enim harum eaque fugit.</p>
								<a href="services-1.html" class="btn btn-default">Read More</a>
							</div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-40" data-animation="fadeInUp">
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/2.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/2.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-2.html">Construction Consultant</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
								deserunt a enim harum eaque fugit.</p>
								<a href="services-2.html" class="btn btn-default">Read More</a>
							</div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-40" data-animation="fadeInRight">
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/3.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/3.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-3.html">House Renovation</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
								deserunt a enim harum eaque fugit.</p>
								<a href="services-3.html" class="btn btn-default">Read More</a>
							</div>
                        </div>
                </div>
				<div class="row">
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-xs-pad-40">
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/5.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/5.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-4.html">Green House</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
								deserunt a enim harum eaque fugit.</p>
								<a href="services-4.html" class="btn btn-default">Read More</a>
							</div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-xs-pad-40">
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/6.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/6.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-5.html">Tiling and Painting</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
								deserunt a enim harum eaque fugit.</p>
								<a href="services-5.html" class="btn btn-default">Read More</a>
							</div>
                        </div>
						<div class="col-sm-4 col-md-4 col-xs-12 bottom-xs-pad-40">
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/4.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/4.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-6.html">Metal Roofing</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
								deserunt a enim harum eaque fugit.</p>
								<a href="services-6.html" class="btn btn-default">Read More</a>
							</div>
                        </div>
                </div>
            </div>
        </section>
        <!-- Services -->
		<section id="quote" class="page-section tb-pad-30 bg-color">
            <div class="container">
				<div class="row">
					<div class="col-md-12 top-pad-10 white" data-animation="fadeInUp">
						<h2 class="heading"> Amazing <span class="black">flexibility</span>, Dark & white Scheme, Powerful Shortcodes, <span class="black">Latest Bootstrap</span> & Ultra Responsive Features makes Metal standout from the crowd.</h2>
					</div>
				</div>
			</div>
        </section>
        <!-- support -->
        <section id="who-we-are" class="page-section new-dark white">
            <div class="container who-we-are">
                <div class="row">
                    <div class="col-md-12 bottom-pad-30 text-center" data-animation="fadeInUp">
                        <!-- Title -->
                        <h2 class="heading"><span class="text-color">About</span> Us</h2>
						
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec
						odio ipsum. Suspendisse cursus malesuada facilisis.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" data-animation="fadeInLeft">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum. Suspendisse cursus
                        malesuada facilisis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum.
                        Suspendisse cursus malesuada facilisis. Suspendisse cursus malesuada facilisis. Nunc consectetur odio sed
                        dolor tincidunt porttitor.</p>
                        <div class="row responsive-features top-pad-20">
                            <!-- Features 1 -->
                            <div class="col-md-6">
                                <!-- Title And Content -->
                                <span class="icon-atom2 text-color"></span>
                                <h5> Our Vision</h5>
                                <p>Lorem ipsum dolor sit amet elit. consectetur Vest</p>
                            </div>
                            <!-- Features 1 -->
                            <!-- Features 2 -->
                            <div class="col-md-6">
                                <!-- Title And Content -->
                                <span class="icon-sun text-color"></span>
                                <h5>Our Mission</h5>
                                <p>Lorem ipsum dolor sit amet elit. consectetur Vest</p>
                            </div>
                            <!-- Features 2 -->
                        </div>
                        <!-- Features -->
                    </div>
                    <div class="col-md-6" data-animation="fadeInRight">
                        <div class="about-us">
							<!-- Progress Bar 1 -->
							<h6>Graphic Design</h6>
							<div class="progress new">
								<div data-percentage="60" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar">
									<span class="progress-label" style="left: 60%">60%</span>
								</div>
							</div>
							<!-- Progress Bar 2 -->
							<h6>HTML 5</h6>
							<div class="progress new">
								<div data-percentage="65" aria-valuemax="100" aria-valuemin="0" aria-valuenow="65" role="progressbar" class="progress-bar">
									<span class="progress-label" style="left: 65%">65%</span>
								</div>
							</div>
							<!-- Progress Bar 3 -->
							<h6>CSS 3</h6>
							<div class="progress new">
								<div data-percentage="80" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar">
									<span class="progress-label" style="left: 80%">80%</span>
								</div>
							</div>
							<!-- Progress Bar 4 -->
							<h6>Wordpress</h6>
							<div class="progress new">
								<div data-percentage="70" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70" role="progressbar" class="progress-bar">
									<span class="progress-label" style="left: 70%">70%</span>
								</div>
							</div>
						</div>                            
					</div>
                </div>
            </div>
        </section>
        <!-- who-we-are -->
        <section id="works" class="page-section white">
			<div class="image-bg content-in fixed" data-background="img/sections/bg/4.jpg"></div>
			<div class="container">				
                <div class="row">
                    <div class="col-md-12 bottom-pad-30 text-center" data-animation="fadeInUp">
                        <!-- Title -->
                        <h2 class="heading">Recent <span class="text-color">Works</span></h2>
                    </div>
                </div>
			</div>
            <div class="work-section">
                <div id="options" class="filter-menu" data-animation="fadeInDown">
                    <ul class="option-set black b-color nav nav-pills">
                        <li class="filter active" data-filter="all">Show All</li>
                        <li class="filter" data-filter=".commercial">Commercial</li>
                        <li class="filter" data-filter=".education">Education</li>
                        <li class="filter" data-filter=".healthcare">Healthcare</li>
                        <li class="filter" data-filter=".residential">Residential</li>
                    </ul>
                </div>
                <!-- filter -->
                <div class="container white general-section" data-animation="fadeInUp">
					<div id="mix-container" class="portfolio-grid new">
						<!-- Item 1 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all commercial">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/1.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/1.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Commercial Building</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 1 Ends-->
						<!-- Item 2 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all commercial education">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/2.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/2.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Corporate Building</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 2 Ends-->
						<!-- Item 3 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all commercial healthcare">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/3.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/3.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Educational University</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 3 Ends-->
						<!-- Item 4 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all education residential">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/4.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/4.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Hospitals</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 4 Ends-->
						<!-- Item 5 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all healthcare commercial">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/5.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/5.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Firm House</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 5 Ends-->
						<!-- Item 6 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all healthcare commercial residential">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/6.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/6.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Sports Ground</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 6 Ends-->
						<!-- Item 7 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all commercial healthcare education">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/7.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/7.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Star Hotels</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 7 Ends-->
						<!-- Item 8 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all commercial residential">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/8.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/8.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Shopping Complex</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 8 Ends-->
						<!-- Item 9 -->
						<div class="grids col-xs-12 col-sm-4 col-md-4 mix all commercial residential">
							<div class="grid box-item">
								<img src="img/sections/portfolio/thumb/3.jpg" width="400" height="273" alt="Recent Work"
								class="img-responsive" />
								<div class="figcaption">
								<!-- Image Popup-->
								<a href="img/sections/portfolio/3.jpg" data-rel="prettyPhoto[portfolio]">
									<i class="fa fa-search"></i>
								</a> 
								<a href="portfolio-single.html">
									<i class="fa fa-link"></i>
								</a></div>
								<div class="caption-block">
									<h4>Residential Flat</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<!-- Item 9 Ends-->
					</div>
					<!-- Mix Container -->
				</div>        
            </div>
        </section>
        <!-- works -->
        <section id="buy-now" class="page-section tb-pad-30 bg-color">
            <div class="container">
				<div class="row">
					<div class="col-md-12 text-center white" data-animation="fadeInUp">
						<h2 class="heading">Don’t wait – Launch your Business or Startup!!</h2>
						<a class="btn btn-transparent-white btn-lg" href="http://themeforest.net/item/metal-mobile-friendly-building-construction-business-template/11445414?ref=zozothemes">BUY METAL NOW</a>
					</div>
				</div>
			</div>
        </section>
        <!-- buy now -->
        <section id="team" class="page-section new-dark white">
            <div class="container">
                <div class="row">
					<div class="col-md-12 bottom-pad-40 text-center" data-animation="fadeInUp">
						<!-- Title -->
						<h2 class="heading">Meet Our <span class="text-color">Team</span></h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec
						odio ipsum. Suspendisse cursus malesuada facilisis.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis.</p>
					</div>
				</div>
                <div class="row text-center">
                    <div class="owl-carousel navigation-1" data-pagination="false" data-items="4" data-autoplay="true"
                    data-navigation="true" data-animation="fadeInUp">
                        <div class="col-sm-6 col-md-3 bottom-xs-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="img/sections/team/1.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Phillip Parisis</h4>
                                    <!-- Designation -->
                                    <div class="role">Project Manager</div>
                                    <!-- Text -->
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.</p>
                                </div>
                                <div class="social-icon">
                                <!-- Social Icons -->
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-google"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-pinterest"></i>
                                </a></div>
                            </div>
                        </div>
                        <!-- .employee  -->
                        <div class="col-sm-6 col-md-3 bottom-xs-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="img/sections/team/2.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Simo Kruyt</h4>
                                    <!-- Designation -->
                                    <div class="role">Construction Manager</div>
                                    <!-- Text -->
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.</p>
                                </div>
                                <div class="social-icon">
                                <!-- Social Icons -->
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-google"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-pinterest"></i>
                                </a></div>
                            </div>
                        </div>
                        <!-- .employee -->
                        <div class="col-sm-6 col-md-3 bottom-xs-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="img/sections/team/3.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Jorge Canaveral</h4>
                                    <!-- Designation -->
                                    <div class="role">Architect</div>
                                    <!-- Text -->
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.</p>
                                </div>
                                <div class="social-icon">
                                <!-- Social Icons -->
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-google"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-pinterest"></i>
                                </a></div>
                            </div>
                        </div>
                        <!-- .employee -->
                        <div class="col-sm-6 col-md-3 bottom-xs-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="img/sections/team/4.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Aimee Devlin</h4>
                                    <!-- Designation -->
                                    <div class="role">Sales Manager</div>
                                    <!-- Text -->
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.</p>
                                </div>
                                <div class="social-icon">
                                <!-- Social Icons -->
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-google"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-pinterest"></i>
                                </a></div>
                            </div>
                        </div>
                        <!-- .employee -->
                        <div class="col-sm-6 col-md-3 bottom-xs-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="img/sections/team/5.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Phillip Parisis</h4>
                                    <!-- Designation -->
                                    <div class="role">Resource Head</div>
                                    <!-- Text -->
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.</p>
                                </div>
                                <div class="social-icon">
                                <!-- Social Icons -->
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-google"></i>
                                </a> 
                                <a href="#">
                                    <i class="fa fa-pinterest"></i>
                                </a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- team -->
        <section id="fun-factor" class="page-section new-semi-dark white">
            <div class="container">
                <div class="row text-right fact-counter">
                    <div class="col-sm-6 col-md-3 bottom-xs-pad-30 fun-icon" data-animation="fadeInUp">
                        <!-- Icon -->
                        <div class="count-number text-color" data-count="92">
                            <span class="counter"></span>
                        </div>
                        <!-- Title -->
                        <h3>Project 
                        <span>Delivered</span></h3>
                    </div>
                    <div class="col-sm-6 col-md-3 bottom-xs-pad-30" data-animation="fadeInDown">
                        <!-- Icon -->
                        <div class="count-number text-color" data-count="83">
                            <span class="counter"></span>
                        </div>
                        <!-- Title -->
                        <h3>Happy 
                        <span>Clients</span></h3>
                    </div>
                    <div class="col-sm-6 col-md-3 bottom-xs-pad-30" data-animation="fadeInUp">
                        <!-- Icon -->
                        <div class="count-number text-color" data-count="67">
                            <span class="counter"></span>
                        </div>
                        <!-- Title -->
                        <h3>Winning 
                        <span>Awards</span></h3>
                    </div>
                    <div class="col-sm-6 col-md-3 bottom-xs-pad-30" data-animation="fadeInDown">
                        <!-- Icon -->
                        <div class="count-number text-color" data-count="36">
                            <span class="counter"></span>
                        </div>
                        <!-- Title -->
                        <h3>Country 
                        <span>Covered</span></h3>
                    </div>
                </div>
            </div>
        </section>
        <!-- fun-factor -->
		<section id="our-support" class="page-section tb-pad-30 bg-color">
            <div class="container">
				<div class="row">
					<div class="col-md-12 top-pad-10 white" data-animation="fadeInUp">
						<h2 class="heading">We would happy to help you solve any issues. If you have any questions, <span class="black">ideas or suggestions</span>, please feel free to contact us at support center.</h2>
					</div>
				</div>
			</div>
        </section>
        <!-- support -->
        <section id="testimonials" class="page-section">
            <div class="image-bg content-in fixed" data-background="img/sections/bg/intro.jpg"></div>            
			<div class="container white">
                <div class="row">
                    <div class="col-sm-12 col-md-8 testimonials">						
						 <div class="row">
							<div class="col-md-12 text-left" data-animation="fadeInUp">
								<!-- Title -->
								<h2 class="heading">What Our <span class="text-color">Customer Says</span></h2>
							</div>
						</div>
                        <div class="owl-carousel pagination-2 text-left color-switch" data-effect="backSlide" data-pagination="true" data-autoplay="true"
                        data-navigation="false" data-singleitem="true" data-animation="fadeInUp">
                            <div class="item">
                                <div class="text-left">
                                    <p>&quot;The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.&quot;</p>
                                </div>
                                <div class="client-details text-left left-align">
                                    <div class="client-image">
                                        <!-- Image -->
                                        <img class="img-circle" src="img/sections/testimonials/1.jpg" width="80" height="80"
                                        alt="" />
                                    </div>
                                    <div class="client-details">
                                    <!-- Name -->
                                    <strong class="text-color">John Doe</strong> 
                                    <!-- Company -->
                                     
                                    Designer, zozothemes</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="text-left">
                                    <p>&quot;The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.&quot;</p>
                                </div>
                                <div class="client-details text-left left-align">
                                    <div class="client-image">
                                        <!-- Image -->
                                        <img class="img-circle" src="img/sections/testimonials/1.jpg" width="80" height="80"
                                        alt="" />
                                    </div>
                                    <div class="client-details">
                                    <!-- Name -->
                                    <strong class="text-color">John Doe</strong> 
                                    <!-- Company -->
                                     
                                    Designer, zozothemes</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="text-left">
                                    <p>&quot;The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                                    intereste.&quot;</p>
                                </div>
                                <div class="client-details text-left left-align">
                                    <div class="client-image">
                                        <!-- Image -->
                                        <img class="img-circle" src="img/sections/testimonials/1.jpg" width="80" height="80"
                                        alt="" />
                                    </div>
                                    <div class="client-details">
                                    <!-- Name -->
                                    <strong class="text-color">John Doe</strong> 
                                    <!-- Company -->
                                     
                                    Designer, zozothemes</div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="col-md-4 text-center border">
                        <a href="#">
                        <img src="img/sections/clients/new/1.png" width="170" height="90" alt="" /></a> 
                        <a href="#"> 
                        <img src="img/sections/clients/new/2.png" width="170" height="90" alt="" /></a> 
                        <a href="#"> 
                        <img src="img/sections/clients/new/3.png" width="170" height="90" alt="" /></a> 
                        <a href="#"> 
                        <img src="img/sections/clients/new/4.png" width="170" height="90" alt="" /></a> 
                        <a href="#">
                        <img src="img/sections/clients/new/5.png" width="170" height="90" alt="" /></a> 
                        <a href="#"> 
                        <img src="img/sections/clients/new/6.png" width="170" height="90" alt="" /></a> 
                    </div>
                
				</div>
            </div>
        </section>
        <!-- testimonials -->    
		<section id="latest-news" class="page-section new-dark white">
            <div class="container">
                 <div class="row">
					<div class="col-md-12 bottom-pad-60 text-center" data-animation="fadeInUp">
						<!-- Title -->
						<h2 class="heading">Our Latest <span class="text-color">News</span></h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec
						odio ipsum. Suspendisse cursus malesuada facilisis.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis.</p>
					</div>
				</div>
                <div class="row lit-space">
                    <div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="3"
                    data-autoplay="true" data-navigation="true" data-animation="fadeInUp">
                        <div class="col-sm-4 col-md-4 col-xs-12">
                            <p class="text-center">
                                <a href="img/sections/services/1.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                    <img src="img/sections/services/1.jpg" width="420" height="280" alt="" />
                                </a>
                            </p>
                            <h4>
                                <a href="blog-single.html">General Contracting</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
                            deserunt a enim harum eaque fugit.</p>
                            <a href="blog-single.html" class="read-more">Read More</a>
                            <div class="right-post-meta">
                            <span class="meta-like">
                            <i class="icon-heart"></i> 5</span> 
                            <span class="meta-comment">
                            <i class="icon-comment"></i> 12</span></div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12">
                            <p class="text-center">
                                <a href="img/sections/services/2.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                    <img src="img/sections/services/2.jpg" width="420" height="280" alt="" />
                                </a>
                            </p>
                            <h4>
                                <a href="blog-single.html">Construction Consultant</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
                            deserunt a enim harum eaque fugit.</p>
                            <a href="#" class="read-more">Read More</a>
                            <div class="pull-right">
                            <i class="icon-heart"></i> 5 
                            <i class="icon-comment"></i> 12</div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12">
                            <p class="text-center">
                                <a href="img/sections/services/3.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                    <img src="img/sections/services/3.jpg" width="420" height="280" alt="" />
                                </a>
                            </p>
                            <h4>
                                <a href="blog-single.html">House Renovation</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
                            deserunt a enim harum eaque fugit.</p>
                            <a href="#" class="read-more">Read More</a>
                            <div class="right-post-meta">
                            <span class="meta-like">
                            <i class="icon-heart"></i> 5</span> 
                            <span class="meta-comment">
                            <i class="icon-comment"></i> 12</span></div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12">
                            <p class="text-center">
                                <a href="img/sections/services/4.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                    <img src="img/sections/services/4.jpg" width="420" height="280" alt="" />
                                </a>
                            </p>
                            <h4>
                                <a href="blog-single.html">Metal Roofing</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
                            deserunt a enim harum eaque fugit.</p>
                            <a href="blog-single.html" class="read-more">Read More</a>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12">
                            <p class="text-center">
                                <a href="img/sections/services/5.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                    <img src="img/sections/services/5.jpg" width="420" height="280" alt="" />
                                </a>
                            </p>
                            <h4>
                                <a href="blog-single.html">Green House</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
                            deserunt a enim harum eaque fugit.</p>
                            <a href="blog-single.html" class="read-more">Read More</a>
                            <div class="right-post-meta">
                            <span class="meta-like">
                            <i class="icon-heart"></i> 5</span> 
                            <span class="meta-comment">
                            <i class="icon-comment"></i> 12</span></div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12">
                            <p class="text-center">
                                <a href="img/sections/services/6.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                    <img src="img/sections/services/6.jpg" width="420" height="280" alt="" />
                                </a>
                            </p>
                            <h4>
                                <a href="blog-single.html">Tiling and Painting</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id pariatur molestiae illum cum facere
                            deserunt a enim harum eaque fugit.</p>
                            <a href="blog-single.html" class="read-more">Read More</a>
                            <div class="right-post-meta">
                            <span class="meta-like">
                            <i class="icon-heart"></i> 5</span> 
                            <span class="meta-comment">
                            <i class="icon-comment"></i> 12</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- news -->
        <section id="buy-now1" class="page-section tb-pad-50">			
			<div class="image-bg content-in fixed" data-background="img/sections/bg/4.jpg"><div class="overlay-color"></div></div>
            <div class="container">
				<div class="row">
					<div class="col-md-12 text-center white" data-animation="fadeInUp">
						<h2 class="heading">A Powerful Contruction Template Here For You!!</h2>
						<a class="btn btn-transparent-white btn-lg" href="http://themeforest.net/item/metal-mobile-friendly-building-construction-business-template/11445414?ref=zozothemes">BUY METAL NOW</a>
					</div>
				</div>
			</div>
        </section>
        <!-- buy now -->
        <footer id="footer">
            <div class="footer-widget new-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                        <div class="widget-title">
                            <!-- Title -->
                            <h3 class="title">METAL</h3>
							<p>Metal is a mobile friendly business template specially designed for Building, Renovation, Remodeling, Handyman business or Construction company and those that offer building services.</p>
							<p>It is ultimate flexible with loads of nice options and features. It’s 100% responsive design was tested on all major handheld devices. </p>
                        </div>
                        <!-- Address -->
                        
						<!-- Social Links -->
						<div class="social-icon new-semi-dark-icon icons-circle i-3x">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a> 
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a> 
							<a href="#">
								<i class="fa fa-pinterest"></i>
							</a> 
							<a href="#">
								<i class="fa fa-google"></i>
							</a> 
							<a href="#">
								<i class="fa fa-linkedin"></i>
							</a>
						</div>
						</div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                            <div class="widget-title">
                                <!-- Title -->
                                <h3 class="title">Services</h3>
                            </div>
                            <nav>
                                <ul class="border-list">
                                    <!-- List Items -->
                                    <li>
                                        <a href="#">General Contracting</a>
                                    </li>
                                    <li>
                                        <a href="#">Construction Consultant</a>
                                    </li>
                                    <li>
                                        <a href="#">House Renovation</a>
                                    </li>
                                    <li>
                                        <a href="#">Metal Roofing</a>
                                    </li>
                                    <li>
                                        <a href="#">Green House</a>
                                    </li>
                                    <li>
                                        <a href="#">Tiling and Painting</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget">
                            <div class="widget-title">
                                <!-- Title -->
                                <h3 class="title">Address</h3>
                            </div>
							<p>
							<strong>Office:</strong> Zozotheme.com
							<br />No. 12, Ribon Building,
							<br />Walse street, Australia.</p>
							<!-- Email -->
							<a class="text-color" href="mailto:info@zozothemes.com">info@zozothemes.com</a> 
							<!-- Phone -->
							<p>
							<strong>Call Us:</strong> +0 (123) 456-78-90 or
							<br />+0 (123) 456-78-90</p>
                            <nav>
                                <ul>
                                    <!-- List Items -->
                                    <li>
                                        <a href="#">Monday-Friday: 9am to 5pm</a>
                                    </li>
                                    <li>
                                        <a href="#">Saturday / Sunday: Closed</a>
                                    </li>
                                </ul>
                                
                            </nav>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget newsletter bottom-xs-pad-20">
                            <div class="widget-title">
                                <!-- Title -->
                                <h3 class="title">Newsletter Signup</h3>
                            </div>
                            <div>
                                <!-- Text -->
                                <p>Subscribe to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</p>
                                <p class="form-message1"></p>
                                <div class="clearfix"></div>
                                <!-- Form -->
                                <form id="subscribe_form" action="subscription.php" method="post" name="subscribe_form"
                                role="form">
                                    <div class="input-text form-group has-feedback">
                                    <input class="form-control" type="email" value="" name="subscribe_email" /> 
                                    <button class="submit bg-color" type="submit">
                                        <span class="glyphicon glyphicon-arrow-right"></span>
                                    </button></div>
                                </form>
                            </div>
							<!-- Count -->
							<div class="footer-count">
								<p class="count-number" data-count="3550">total projects : 
								<span class="counter"></span></p>
							</div>
							<div class="footer-count">
								<p class="count-number" data-count="2550">happy clients : 
								<span class="counter"></span></p>
							</div>                            
                        </div>
                        <!-- .newsletter -->
                    </div>
                </div>
            </div>
            <!-- footer-top -->
            <div class="copyright new-semi-dark">
                <div class="container">
                    <div class="row">
                        <!-- Copyrights -->
                        <div class="col-md-12">Copyright &copy; zozothemes.com., 2015
                        <br />
                        <!-- Terms Link -->
                         
                        <a href="#">Terms of Use</a> / 
                        <a href="#">Privacy Policy</a>
						<div class="position-right page-scroll new-semi-dark-icon icons-circle i-3x">
							<!-- Goto Top -->
                            <a href="#page">
                                <i class="glyphicon glyphicon-arrow-up no-margin"></i>
                            </a>
						</div>
						</div>
                    </div>
                </div>
            </div>
            <!-- footer-bottom -->
        </footer>
        <!-- footer -->
    </div>