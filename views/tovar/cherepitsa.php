<?php $this->title = "Черепицца" ?>

 <div class="container shop">
                <div class="section-title text-left">
                    <h2 class="title">Металлочерепица</h2>
                </div>
                <div class="row">
                    <div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="4"
                    data-autoplay="true" data-navigation="true">
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/7.jpg" alt="" width="640" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="#"><h4>Каскад</h4></a>
                                    <h5 class="text-color">$199</h5>
                                </div>

                            </div>
                        </div>
                        <!-- .product -->
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="#">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/8.jpg" alt="" width="640" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Монтеррей</h4></a>
                                    <h5 class="text-color">$229</h5>
                                </div>

                            </div>
                        </div>
                        <!-- .product -->
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/9.jpg" alt="" width="640" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Супермонтерей</h4></a>
                                    <h5 class="text-color">$189</h5>
                                </div>

                            </div>
                        </div>
                        <!-- .product -->
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/13.jpg" alt="" width="640" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Гранд Лайн</h4></a>
                                    <h5 class="text-color">$79</h5>
                                </div>

                            </div>
                        </div>


                           <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/10.jpg" alt="" width="640" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Монтеррей-люкс</h4></a>
                                    <h5 class="text-color">$79</h5>
                                </div>

                            </div>
                        </div>

                          <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/11.jpg" alt="" width="640" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Декоррей</h4></a>
                                    <h5 class="text-color">$79</h5>
                                </div>

                            </div>
                        </div>

                          <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/12.jpg" alt="" width="640" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Финнера</h4></a>
                                    <h5 class="text-color">$79</h5>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div role="tabpanel"  style="margin:60px;">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Черепица</a>
                                        </li>

                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <h5>Та'риф</h5>
                                            <p>

Уйингизда ва бошқа том ёпиш ишларини тез ва арзон нархларда бажаришингиз керакми? Металл чинни танлашингиз! Бизда сиз Тошкент шаҳридаги металлни ишлаб чиқарувчилар томонидан сотиб олишингиз мумкин. Биз ўз ишлаб чиқаришимиз Каскад ва Монтеррей номли металл профилларини, шунингдек, бошқа металл кошинларини сотамиз. Арзон нарх ва юқори сифат.

</p><p>
Уйларининг чидамлилиги ва ишончлилиги ҳақида ўйлаётган кўп одамлар томни металл каби материаллардан танлашади. У кўп йиллардан буён машҳур бўлган сифатлари туфайли уй эгаларининг имтиёзларини қўлга киритди.
</p>
                                            <ul class="list-style">
                                                <li>Металлочерепица Каскад</li>
                                                <li>Металлочерепица Монтеррей</li>
                                                <li>Металлочерепица Супермонтерей</li>
                                                <li>Металлочерепица Монтеррей-люкс</li>
                                                <li>Металлочерепица Декоррей</li>
                                                <li>Металлочерепица Финнера</li>
                                                <li>Металлочерепица Гранд Лайн</li>
                                            </ul>
                                            <p>Профилли листлар ишлаб чиқаришда галванизли қатламли пўлатдан РАЛ ва РР столига ёки ҳимоя қопламасиз қўлланиладиган полимер ёки бўёқ қопламаси қўлланилади. Полимер қопламаси ҳимоя функтсияга (коррозияни олдини олади), эстетик функтсияга (рангли эчимларнинг бойлиги турли меъморий объектларни қуриш учун профилли плиталардан фойдаланиш имконини беради). </p>
                                            <p>Нима учун Тошкентдаги металл  (ишлаб чиқарувчининг нархи) шу қадар талаб қилинади? </p>
                                            <ul class="list-style">
                                            <li>Биринчидан, барча маҳсулотларимиз жуда мустаҳкам (20 йилдан ортиқ!).</li>
                                            <li>Иккинчидан, фабрикамиздаги ҳар қандай партия бракдан қочиш учун алоҳида назорат қилинади.</li>
                                            <li>Учинчидан, юқори сифатли хомашё ва вақт синовидан ўтган қоплама бизнинг томимизни коррозияга ёки ташқи таъсирларга қарши туришга яроқсиз ҳолга келтиради!</li>
                                            <li>Тўртинчидан, биз ўз вақтида етказиб берувчилар сифатида кўрсатдик: этказиб бериш ва тўғридан-тўғри уйингизга жўнатиш ўз вақтида амалга оширилади!</li>
                                            <li>Ва, ниҳоят, бизнинг металл кафелимизнинг асосий афзалликларидан бири бу нархдир! Сотувчи ёки бошқа ноаниқ фойда билан ишлаш учун ортиқча тўлов шарт эмас</li>
                                            </ul>
                                        </div>




        <div class="page-header page-title-left page-title-pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title">Рулонлар</h1>
                        <h5>Лист</h5>

                    </div>
                </div>
            </div>
        </div>


         <section id="who-we-are" class="page-section no-pad border-tb">
            <div class="container-fluid who-we-are">
                <div class="row">
                    <div class="col-md-8 pad-60 xs-pad-0 bottom-pad-10 top-pad-80">
                        <div class="owl-carousel navigation-3 opacity text-left" data-pagination="false" data-items="2"
                        data-autoplay="true" data-navigation="true">
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/1.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/1.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Рулонлар</a>
                                </h3>
                                <p>профилланган тахта, сандвич панели, металл, текис плиталар, қўшимча элементлар, дренаж тизими, ишлаб чиқариш учун.</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/2.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/2.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Галванизланган рулонлар</a>
                                </h3>
                                <p>Юқоридаги стандартлар бўйича ишлаб чиқарилган рулонларда галваниз пўлатдир, аксарият ҳолларда бир-бирининг ўрнини босадиган. Агар сиз маълум бир стандартга мувофиқ синк сотиб олишни талаб қилсангиз, ҳисоб-фактурани юборганингизда, ҳозирги пайтда маҳсулотнинг қайси стандартда мавжудлиги ҳақида менежер билан текширишни унутманг.     </p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/3.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/3.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Иссиқурама рулонлар</a>
                                </h3>
                                <p>Юқоридаги стандартлар бўйича ишлаб чиқарилган рулонларда галваниз пўлатдир, аксарият ҳолларда бир-бирининг ўрнини босадиган. Агар сиз маълум бир стандартга мувофиқ синк сотиб олишни талаб қилсангиз, ҳисоб-фактурани юборганингизда, ҳозирги пайтда маҳсулотнинг қайси стандартда мавжудлиги ҳақида менежер билан текширишни унутманг.
</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/4.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/4.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Совуқурама рулонлар</a>
                                </h3>
                                <p>Совуқурама рулонлар ишлаб чиқариш совуқ сақловчанлик усулига асосланган. Унинг хомашёси шишани олиб ташлаш учун қайноқ чизиғига олдиндан қадоқланган иссиқ қайишли рулон. Пўлат совуқ руло қалинлиги 0,3-3,5 мм қалинликда ишлаб чиқарилади, рулоннинг одатий оғирлиги 6-10 тоннани ташкил қилади, стандарт кенглик 1250 мм.</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/5.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/5.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Иссиқурама рулонлар ишлаб чиқариш</a>
                                </h3>
                                <p>Иссиқурама рулонларни ишлаб чиқариш икки хил турдаги пўлатдан фойдаланишни ўз ичига олади - оддий умумий мақсад ва юқори сифатли углерод. Шунга мувофиқ: қуйи қотишма ва юқори қотишма.</p>
                            </div>
                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/6.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/6.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Иссиқурама рулонларни қўллаш</a>
                                </h3>
                                <p>Темир-бетон конструктсиялари ва маҳсулотларини мустаҳкамлаш учун мўлжалланган мустаҳкамлашни ишлаб чиқариш учун 4-8 мм қалинликдаги пўлат тцилиндрни ишлатиш мумкин. Гурра тахта, металл қоплама, девор ва том ёпиш сандиғи панелларини ишлаб чиқаришда ёрдамчи материал бўлган бошқа ёғоч, бурчаклардан ёғли иссиқ қайишли чизиқлар ишлаб чиқариш учун фойдаланиладиган 2-4 мм материалнинг қалинлиги. Ҳар бир алоҳида ҳолатда сиз сотиб олишингиз керак бўлган қайноқ рулман қандай қарорга келтирилган.</p>
                            </div>
                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/7.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/7.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Рулон Х/К</a>
                                </h3>
                                <p>Совутгичли пўлатдан рулонларда мижозларимиз доимо талабга эга. Асосан металл буюмлар ишлаб чиқариш учун матбаа сифатида ишлатилади, бüкме ва пайвандлаш.
</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 no-pad text-center">
                        <!-- Image -->
                        <div class="image-bg" data-background="<?=Yii::getAlias('@web')?>/img/sections/bg/1.jpg"></div>
                    </div>
                </div>
            </div>
        </section></div>
        <!-- who-we-are -->
