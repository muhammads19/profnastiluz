<?php $this->title = "Сендвич панеллар" ?>

        <!-- Sticky Navbar -->
        <div class="page-header page-title-left page-title-pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title">Сендвич панеллар</h1>
                        <h5>Ишлаб чиқариш ва қадоқлаш</h5>

                    </div>
                </div>
            </div>
        </div>
        <!-- page-header -->
        <section id="services" class="page-section service-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-9 col-md-push-3">
                        <div class="row">
                            <div class="col-md-12 content-block">
                                <div class="text-center">
                                    <div class="owl-carousel navigation-4" data-pagination="false" data-items="1"
                                    data-singleitem="true" data-autoplay="true" data-navigation="true">
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/13.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/13.jpg" width="1000" height="500" alt="" />
                                    </a>
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/14.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/14.jpg" width="1000" height="500" alt="" />
                                    </a></div>
                                </div>
                                <p>Сендвич панел (базалт толали) минерал ватадан, префабрик бинолар қуришда енг кенг тарқалган қурилиш материаллардан биридир.Бошқа турдаги панелларга нисбатан уларнинг асосий афзалликлари - ёнғинга чидамлилиги оширилган. Бу унинг асосий устунлиги ҳисобланади.Ва бу уни кенг қамровли қуллаш имкониятини беради.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 content-block opacity">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/5.jpg" width="1000" height="500" alt="" />
                                <h4>Қадоқлаш</h4>
                                <p>Сендвич панел деворга-оралиқларга-томларни ёпишда фойдаланишга мулжалланган ишлатилади. Нормал шароитда панелларни -65 +75  гача булган муҳитда қуллашга мулжалланган Панелнинг ички юза ҳароратининг нисбий намлиги 60%гачадир. </p>
                            </div>
                            <div class="col-md-6 content-block opacity">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/6.jpg" width="1000" height="500" alt="" />
                                <h4>Омборхона</h4>
                                <p>Сравнительная характеристика стеновых сэндвич-панелей с другими стеновыми материалами по теплопроводности

                            </p>

                            <ul><h5>Утеплитель</h5>
                                <li>Пенополистирол  </li>

                            <li>Базальтовая вата </li>

                            <li>Древесина </li>
                            <li>Ячеистый бетон </li>
                            <li>Кирпичная кладка </li>


                            </ul>

                            </div>
                        </div>
                        <hr class="top-margin-0" />
                        <div class="row">
                            <div class="col-md-6 service-list content-block">
                                <h4>Нима учун бизни танлашади?</h4>
                                <ul>
                                    <li>
                                        <i class="icon-alarm3 text-color"></i>
                                        <p>биз дам олиш кунларисиз ишлаймиз</p>
                                    </li>
                                    <li>
                                        <i class="icon-shield2 text-color"></i>
                                        <p>Узоқ йиллик тажриба егамиз.</p>
                                    </li>
                            </div>
                            <div class="col-md-6 content-block">
                                <div role="tabpanel">
                                    <!-- Nav tabs -->

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active top-margin-0" id="brochure">
                                            <a href="#">
                                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/broucher.jpg" width="450" height="270" alt="" />
                                            </a>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="reporting">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="inspection">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="others">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-sm-12 col-md-3 col-md-pull-9">
                        <div class="widget list-border">
                            <div class="widget-title">
                                <h3 class="title">Ҳамма Товарлар</h3>
                            </div>
                            <div id="MainMenu1">
                                <div class="list-group panel">
                                    <div class="collapse in" id="demo">
                                    <a href="profnastil.php" class="list-group-item">Профнастил</a>
                                    <a href="tunukapon.php" class="list-group-item">Тунукапон</a>
                                    <a href="sendvich.php" class="list-group-item">Сендвич панеллар</a>
                                    <a href="potokli.php" class="list-group-item">Потокли ва Фасадли</a>
                                    <a href="panjara.php" class="list-group-item active">Панжара</a>
                                    <a href="cherepitsa.php" class="list-group-item">Черепица</a></div>
                                </div>
                            </div>
                            <!-- category-list -->
                        </div>
                        <div class="widget">
                            <div class="widget-title">
                                <h3 class="title">Download Brochures</h3>
                            </div>
                            <div id="MainMenu2">
                                <div class="list-group list-icons panel">
                                    <div class="collapse in" id="demo1">
                                    <a href="#" class="list-group-item pdf">General Construction
                                    <i class="fa fa-file-pdf-o"></i></a>
                                    <a href="service-1.html" class="list-group-item pdf">Architectural Model
                                    <i class="fa fa-file-pdf-o"></i></a>
                                    <a href="service-1.html" class="list-group-item pdf">Layout Designs
                                    <i class="fa fa-file-pdf-o"></i></a>
                                    <a href="service-1.html" class="list-group-item pdf">Go Green
                                    <i class="fa fa-file-pdf-o"></i></a></div>
                                </div>
                                <!-- category-list -->
                            </div>
                            <div class="widget">
                                <div class="widget-title">
                                    <h3 class="title">Gallery</h3>
                                </div>
                                <div class="owl-carousel navigation-4" data-pagination="false" data-items="1" data-autoplay="true"
                                data-navigation="true">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/1.jpg" width="270" height="270" alt="" />
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/2.jpg" width="270" height="270" alt="" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services -->
        <div id="get-quote" class="bg-color black text-center">
            <div class="container">
                <div class="row get-a-quote">
                    <div class="col-md-12">Get A Free Quote / Need a Help ?
                    <a class="black" href="#">Contact Us</a></div>
                </div>
                <div class="move-top bg-color page-scroll">
                    <a href="#page">
                        <i class="glyphicon glyphicon-arrow-up"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- request -->
