        <!-- Sticky Navbar -->
        <div class="page-header page-title-left page-title-pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title">Tiling and Painting</h1>
                        <h5>A Short Page title</h5>
                        <ul class="breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a href="#">Services</a>
                            </li>
                            <li class="active">Tiling and Painting</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- page-header -->
        <section id="services" class="page-section service-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-9 col-md-push-3">
                        <div class="row">
                            <div class="col-md-12 content-block">
                                <div class="text-center">
                                    <div class="owl-carousel navigation-4" data-pagination="false" data-items="1"
                                    data-singleitem="true" data-autoplay="true" data-navigation="true">
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/13.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/13.jpg" width="1000" height="500" alt="" />
                                    </a> 
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/14.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/14.jpg" width="1000" height="500" alt="" />
                                    </a></div>
                                </div>
                                <p>Vivamus elementum laoreet lorem maecen as er felis sed mollis semper lobortis vitae phasellus
                                commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis. Donec
                                facilisis. Nunc euismod. Suspendisse potenti. Nullam at ante.Morbi consequat lobortis eros.
                                Phasellus vehicula auctor nisi. Donec vestibulum odio. Morbi ut risus nec orci bibendum
                                commodo.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 content-block opacity">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/5.jpg" width="1000" height="500" alt="" />
                                <h4>THE INITIAL PLANNING</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et magna lacinia, congue enim et,
                                dignissim ante. Nam gravida sit amet odio eu tincidunt.</p>
                            </div>
                            <div class="col-md-6 content-block opacity">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/6.jpg" width="1000" height="500" alt="" />
                                <h4>Project Process</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et magna lacinia, congue enim et,
                                dignissim ante. Nam gravida sit amet odio eu tincidunt.</p>
                            </div>
                        </div>
                        <hr class="top-margin-0" />
                        <div class="row">
                            <div class="col-md-6 service-list content-block">
                                <h4>why choose us</h4>
                                <ul>
                                    <li>
                                        <i class="icon-alarm3 text-color"></i>
                                        <p>We available 24/7 feel free to contact us.</p>
                                    </li>
                                    <li>
                                        <i class="icon-shield2 text-color"></i>
                                        <p>We are genius because of experience.</p>
                                    </li>
                                    <li>
                                        <i class="icon-price-tag text-color"></i>
                                        <p>Offer low price compare with other builders</p>
                                    </li>
                                    <li>
                                        <i class="icon-headphones text-color"></i>
                                        <p>We provide free estimation for all projects</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 content-block">
                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#brochure" aria-controls="brochure" role="tab" data-toggle="tab">Brochure</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#reporting" aria-controls="reporting" role="tab"
                                            data-toggle="tab">Reporting</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#inspection" aria-controls="inspection" role="tab"
                                            data-toggle="tab">Inspection</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#others" aria-controls="others" role="tab" data-toggle="tab">Others</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active top-margin-0" id="brochure">
                                            <a href="#">
                                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/broucher.jpg" width="450" height="270" alt="" />
                                            </a>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="reporting">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="inspection">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="others">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-sm-12 col-md-3 col-md-pull-9">
                        <div class="widget list-border">
                            <div class="widget-title">
                                <h3 class="title">All Services</h3>
                            </div>
                            <div id="MainMenu1">
                                <div class="list-group panel">
                                    <div class="collapse in" id="demo">
                                    <a href="services-1.html" class="list-group-item">General Construction</a> 
                                    <a href="services-2.html" class="list-group-item">Construction Consultant</a> 
                                    <a href="services-3.html" class="list-group-item">House Renovation</a> 
                                    <a href="services-4.html" class="list-group-item">Green House</a> 
                                    <a href="#" class="list-group-item active">Tiling and Painting</a> 
                                    <a href="services-6.html" class="list-group-item">Metal Roofing</a></div>
                                </div>
                            </div>
                            <!-- category-list -->
                        </div>
                        <div class="widget">
                            <div class="widget-title">
                                <h3 class="title">Download Brochures</h3>
                            </div>
                            <div id="MainMenu2">
                                <div class="list-group list-icons panel">
                                    <div class="collapse in" id="demo1">
                                    <a href="#" class="list-group-item pdf">General Construction 
                                    <i class="fa fa-file-pdf-o"></i></a> 
                                    <a href="service-1.html" class="list-group-item pdf">Architectural Model 
                                    <i class="fa fa-file-pdf-o"></i></a> 
                                    <a href="service-1.html" class="list-group-item pdf">Layout Designs 
                                    <i class="fa fa-file-pdf-o"></i></a> 
                                    <a href="service-1.html" class="list-group-item pdf">Go Green 
                                    <i class="fa fa-file-pdf-o"></i></a></div>
                                </div>
                                <!-- category-list -->
                            </div>
                            <div class="widget">
                                <div class="widget-title">
                                    <h3 class="title">Gallery</h3>
                                </div>
                                <div class="owl-carousel navigation-4" data-pagination="false" data-items="1" data-autoplay="true"
                                data-navigation="true">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/1.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/1.jpg" width="270" height="270" alt="" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services -->
        <div id="get-quote" class="bg-color black text-center">
            <div class="container">
                <div class="row get-a-quote">
                    <div class="col-md-12">Get A Free Quote / Need a Help ? 
                    <a class="black" href="#">Contact Us</a></div>
                </div>
                <div class="move-top bg-color page-scroll">
                    <a href="#page">
                        <i class="glyphicon glyphicon-arrow-up"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- request -->
       